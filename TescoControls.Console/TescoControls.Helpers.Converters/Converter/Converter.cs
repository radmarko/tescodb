﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace TescoControls.Helpers.Converter.ObjectDictionary
{
    public class A
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<B> ListOfB { get; set; }
        public Dictionary<string, C> DictionaryOfC { get; set; }
    }

    public class B
    {
        public int Id { get; set; }
        public C CValueData { get; set; }
    }

    public class C
    {
        public int Value { get; set; }
        public string Data { get; set; }
    }
    public static class Converter
    {
        public static Dictionary<string, Node> ObjectToDictionary(Object _object, Type type)
        {
            var result = new Dictionary<string, Node>();

            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                var node = new Node 
                {
                    Name = property.Name,
                    Type = property.PropertyType
                };

                if (property.PropertyType == typeof(string) || property.PropertyType.IsPrimitive)
                {
                    node.Value = property.GetValue(_object);
                    node.Nodes = null;
                }
                //else if (property.PropertyType.IsGenericType)
                //{
                //    if (property.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                //    {
                //        var keyType = property.PropertyType.GetGenericArguments()[0];
                //        var valueType = property.PropertyType.GetGenericArguments()[1];
                //    }
                //    else if (property.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                //    {
                //        var itemType = property.PropertyType.GetGenericArguments()[0];
                //    }
                //}
                else if (property.PropertyType.IsClass)
                {
                    node.Value = null;
                    node.Nodes = ObjectToDictionary(property.GetValue(_object), property.PropertyType);
                }

                result.Add(node.Name, node);
            }

            return result;
        }

        public static object DictionaryToObject(Dictionary<string, Node> dictionary, Type type)
        {
            var result = Activator.CreateInstance(type);
            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                if (property.PropertyType == typeof(string) || property.PropertyType.IsPrimitive)
                {
                    property.SetValue(result, dictionary[property.Name].Value);
                }
                else if (property.PropertyType.IsClass)
                {
                    property.SetValue(result, DictionaryToObject(dictionary[property.Name].Nodes, property.PropertyType));
                }
            }

            return result;
        }

        public static dynamic DictionaryToExpandoObject(Dictionary<string, Node> dictionary)
        {
            IDictionary<string, object> result = new ExpandoObject();

            foreach (var item in dictionary)
            {
                if (item.Value.Type == typeof(string) || item.Value.Type.IsPrimitive)
                {
                    result.Add(item.Key, item.Value.Value); //todo: funny naming convention, fix this
                }
                else if (item.Value.Type.IsClass)
                {
                    result.Add(item.Key, DictionaryToExpandoObject(item.Value.Nodes));
                }
            }

            return result;
        }
    }
}
