﻿using System;
using System.Collections.Generic;

namespace TescoControls.Helpers.Converter.ObjectDictionary
{
    public class Node
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public Type Type { get; set; }
        public Dictionary<string, Node> Nodes { get; set; }
    }
}
