﻿using Nancy.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TescoControls.Helpers.Converters.JavaScriptConverter
{
    public static class JavaScriptConverter2
    {
        private static Dictionary<string, object> DeserializeJson(string json)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            Dictionary<string, object> dictionary =
                serializer.Deserialize<Dictionary<string, object>>(json);

            ResolveEntry(dictionary, "Base");

            return dictionary;
        }

        private static void ResolveEntry(Dictionary<string, object> dic, string SupKey)
        {
            // Each entry in the main dictionary is [Table-Key , Table-Value]
            foreach (KeyValuePair<string, object> entry in dic)
            {
                if (entry.Value is Dictionary<string, object>)  // for Meta and Data
                    ResolveEntry((Dictionary<string, object>)entry.Value, entry.Key);
                else
                    if (entry.Value is ICollection)

                // Checks whether the current Table-Value in Table is Sub-Dictionary|Collection|Flat Object
                {
                    foreach (var item in (ICollection)entry.Value)

                    // If the table base value is a collection of items then loop through them
                    {
                        if (item is Dictionary<string, object>)

                            // If the Collection-Item is a Dictionary then submit it for resolving
                            ResolveEntry((Dictionary<string, object>)item, SupKey + " : " + entry.Key);
                        else
                            Console.WriteLine(item.ToString());

                        // If the Collection-Item is a Flat Object then output
                    }
                }
                else
                    Console.WriteLine(SupKey + " : " +
                        entry.Key.ToString() + "--->" + entry.Value.ToString());

                // The Current Table-Value is Flat Object
            }
        }

        public static string ObjectToJson<T>(T _object)
        {
            var result = new JavaScriptSerializer().Serialize(_object);

            return result;
        }

        public static Dictionary<string, object> JsonToDictionary(string jsonObject)
        {
            var result = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(jsonObject);

            return result;
        }
    }
}
