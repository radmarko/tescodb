﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Dynamic;
using System.IO;

namespace TescoControls.Helpers.Converters.JsonConverter
{
    public static class JsonConverter
    {
        public static string ObjectToJson<T>(T _object)
            where T : class
        {
            var result = JsonConvert.SerializeObject(_object);

            return result;
        }
        
        public static T JsonToObject<T>(string jsonObject)
            where T : class
        {
            var result = JsonConvert.DeserializeObject<T>(jsonObject);

            return result;
        }

        public static object JsonToObject(string jsonObject, Type type)
        {
            var result = JsonConvert.DeserializeObject(jsonObject, type);

            return result;
        }

        public static dynamic JsonToExpandoObject(string jsonObject)
        {
            dynamic result = JsonConvert.DeserializeObject<ExpandoObject>(jsonObject);

            return result;
        }

        public static T ObjectFromJsonFile<T>(string jsonFilePath)
            where T : class
        {
            // read JSON directly from a file
            using (StreamReader file = File.OpenText(jsonFilePath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject jObject = (JObject)JToken.ReadFrom(reader);

                var result = jObject.ToObject<T>();

                return result;
            }
        }

        public static void ObjectToJsonFile<T>(T _object, string jsonFilePath)
            where T : class
        {
            // serialize JSON directly to a file
            using (StreamWriter file = File.CreateText(jsonFilePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, _object);
            }
        }

        public static object ObjectToClassObject(object _object, Type type)
        {
            var jsonValue = JsonConvert.SerializeObject(_object);
            var value = JsonConvert.DeserializeObject(jsonValue, type);

            return value;
        }
    }
}
