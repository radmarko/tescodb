﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TescoControls.Helpers.Converters.IterativeConverter
{
    public static class IterativeConverter
    {
        public static Dictionary<string, object> JsonToDictionary(string jsonObject)
        {
            var result = new Dictionary<string, object>();
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonObject);

            foreach (var item in dictionary)
            {
                if (item.Value.ToString().Contains("{") && item.Value.ToString().Contains("}"))
                    result.Add(item.Key, JsonToDictionary(item.Value.ToString()));
                else
                    result.Add(item.Key, item.Value);
            }

            return result;
        }
    }
}
