﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TescoControls.Client
{
    public static class RestApiClient
    {
        public static async Task<HttpResponseMessage> GetHttpRequestAsync(string requestUri)
        {
            using (var httpClient = new HttpClient())
            {
                var result = await httpClient.GetAsync(requestUri);

                return result;
            }
        }

        public static async Task<HttpResponseMessage> PostHttpRequestAsync<T>(string requestUri, T bodyObject)
        {
            using (var httpClient = new HttpClient())
            {
                var jsonObject = JsonConvert.SerializeObject(bodyObject);
                var data = new StringContent(jsonObject, Encoding.UTF8, "application/json");

                var result = await httpClient.PostAsync(requestUri, data);

                return result;
            }
        }

        public static async Task<HttpResponseMessage> DeleteHttpRequestAsync(string requestUri)
        {
            using (var httpClient = new HttpClient())
            {
                var result = await httpClient.DeleteAsync(requestUri);

                return result;
            }
        }
    }
}
