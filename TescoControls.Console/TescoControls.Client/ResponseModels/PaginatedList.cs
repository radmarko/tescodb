﻿using System.Collections.Generic;

namespace TescoControls.Client.ResponseModels
{
    public class PaginatedList<T>
    {
        public List<T> Records { get; set; }
        public int Count { get; set; }
    }
}
