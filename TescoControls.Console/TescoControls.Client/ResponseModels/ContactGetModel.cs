﻿using System;

namespace TescoControls.Client.ResponseModels
{
    public class ContactGetModel
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Pager { get; set; }
        public string Title { get; set; }
        public string AlternatePhone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Extension { get; set; }
        public string AlternatePhoneExtension { get; set; }
        public string Department { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string ContactType { get; set; }
    }
}
