﻿namespace TescoControls.Client.ResponseModels
{
    public class ContactModel : ContactGetModel
    {
        public string AlternateFirstAddress1 { get; set; }
        public string AlternateFirstAddress2 { get; set; }
        public string AlternateFirstCity { get; set; }
        public string AlternateFirstState { get; set; }
        public string AlternateFirstZip { get; set; }
        public string AlternateSecondAddress1 { get; set; }
        public string AlternateSecondAddress2 { get; set; }
        public string AlternateSecondCity { get; set; }
        public string AlternateSecondState { get; set; }
        public string AlternateSecondZip { get; set; }
        public string History { get; set; }
        public string Login { get; set; }
        public bool? TrimaxContact { get; set; }
    }
}
