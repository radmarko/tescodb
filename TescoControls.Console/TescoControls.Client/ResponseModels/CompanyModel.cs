﻿namespace TescoControls.Client.ResponseModels
{
    public class CompanyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
