﻿namespace TescoControls.Client.RequestModels
{
    public class QueryModel
    {
        public string Search { get; set; }
        public string SortField { get; set; }
        public bool? SortOrder { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
}
