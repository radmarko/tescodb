﻿using MailChimp.Net.Core;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TescoControls.Client.Exceptions;

namespace TescoControls.Client.HttpClients
{
    public class BaseHttpClient
    {
        private const string BASE_PATH = "https://localhost:44397//";

        protected async Task<HttpResponseMessage> GetHttpRequestAsync(string relativeUri)
        {
            var requestUri = BASE_PATH + relativeUri;
            var result = await RestApiClient.GetHttpRequestAsync(requestUri);

            return result;
        }

        protected async Task<HttpResponseMessage> DeleteHttpRequestAsync(string relativeUri)
        {
            var requestUri = BASE_PATH + relativeUri;
            var result = await RestApiClient.DeleteHttpRequestAsync(requestUri);

            return result;
        }

        protected async Task<HttpResponseMessage> PostHttpRequestAsync<T>(string relativeUri, T bodyObject)
        {
            var requestUri = BASE_PATH + relativeUri;
            var result = await RestApiClient.PostHttpRequestAsync(requestUri, bodyObject);

            return result;
        }

        protected async Task<T> GetHttpResponseMessageContent<T>(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<T>(content);

                return result;
            }

            throw new GenericHttpRequestException(); //todo: parse error code and return proper exception
        }

        protected string ObjectToQueryString<T>(T _object)
        {
            var result = string.Join("&", typeof(T)
                               .GetProperties()
                               .Where(p => Attribute.IsDefined(p, typeof(QueryStringAttribute)) && p.GetValue(_object, null) != null)
                               .Select(p => $"{Uri.EscapeDataString(p.Name)}={Uri.EscapeDataString(p.GetValue(_object).ToString())}"));
            
            return result;
        }
    }
}
