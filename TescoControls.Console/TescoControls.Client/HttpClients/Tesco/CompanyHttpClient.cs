﻿using System.Threading.Tasks;
using TescoControls.Client.RequestModels;
using TescoControls.Client.RequestModels.Tesco;
using TescoControls.Client.ResponseModels;

namespace TescoControls.Client.HttpClients
{
    public class CompanyHttpClient : BaseHttpClient
    {
        private const string relativePath = "tesco/companies";

        public async Task<int> CreateAsync(CompanyCreateUpdateModel model)
        {
            var response = await PostHttpRequestAsync(relativePath, model);
            var result = await GetHttpResponseMessageContent<int>(response);
            
            return result;
        }

        public async Task<CompanyModel> GetAsync(int id)
        {
            var response = await GetHttpRequestAsync(relativePath + "/" + id);
            var result = await GetHttpResponseMessageContent<CompanyModel>(response);

            return result;
        }

        public async Task<PaginatedList<CompanyModel>> GetAsync(int id, QueryModel model)
        {
            var queryString = ObjectToQueryString(model);
            var response = await GetHttpRequestAsync(relativePath + "/" + id + queryString);
            var result = await GetHttpResponseMessageContent<PaginatedList<CompanyModel>>(response);

            return result;
        }

        public async Task<bool> UpdateAsync(int id, CompanyCreateUpdateModel model)
        {
            var response = await PostHttpRequestAsync(relativePath + "/" + id, model);
            var result = await GetHttpResponseMessageContent<bool>(response);

            return result;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var response = await DeleteHttpRequestAsync(relativePath + "/" + id);
            var result = await GetHttpResponseMessageContent<bool>(response);

            return result;
        }
    }
}
