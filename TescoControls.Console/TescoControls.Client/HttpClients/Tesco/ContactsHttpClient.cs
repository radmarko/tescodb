﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TescoControls.Client.RequestModels;
using TescoControls.Client.RequestModels.Tesco;
using TescoControls.Client.ResponseModels;

namespace TescoControls.Client.HttpClients
{
    public class ContactsHttpClient : BaseHttpClient
    {
        private const string relativePath = "tesco/contacts";

        public async Task<int> CreateAsync(ContactCreateUpdateModel model)
        {
            var response = await PostHttpRequestAsync(relativePath, model);
            var result = await GetHttpResponseMessageContent<int>(response);

            return result;
        }

        public async Task<ContactModel> GetAsync(int id)
        {
            var response = await GetHttpRequestAsync(relativePath + "/" + id);
            var result = await GetHttpResponseMessageContent<ContactModel>(response);

            return result;
        }

        public async Task<PaginatedList<ContactModel>> GetAsync(int id, QueryModel model)
        {
            var queryString = ObjectToQueryString(model);
            var response = await GetHttpRequestAsync(relativePath + "/" + id + queryString);
            var result = await GetHttpResponseMessageContent<PaginatedList<ContactModel>>(response);

            return result;
        }

        public async Task<bool> UpdateAsync(int id, ContactCreateUpdateModel model)
        {
            var response = await PostHttpRequestAsync(relativePath + "/" + id, model);
            var result = await GetHttpResponseMessageContent<bool>(response);

            return result;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var response = await DeleteHttpRequestAsync(relativePath + "/" + id);
            var result = await GetHttpResponseMessageContent<bool>(response);

            return result;
        }
    }
}
