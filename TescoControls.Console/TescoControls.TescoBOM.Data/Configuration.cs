﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using TescoControls.TescoBOM.Data.Context;
using TescoControls.TescoBOM.Data.Repositories;
using TescoControls.TescoBOM.Data.UnitOfWork;

namespace TescoControls.TescoBOM.Data
{
    public static class Configuration
    {
        public static IConfigurationBuilder AddTescoBOMDataJsonFiles(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder
                .AddJsonFile("appsettings.tescobom-data.json",
                    optional: false,
                    reloadOnChange: true);

            return configurationBuilder;
        }

        public static IServiceCollection ConfigureTescoBOMDataServices(
            this IServiceCollection services,
            IConfiguration configuration,
            bool isDevelopment)
        {
            if (isDevelopment)
            {
                services
                   .AddDbContext<TescoBOMContext>(options =>
                   {
                       options.UseSqlServer(configuration.GetConnectionString("TescoBOMDev"));
                   }, ServiceLifetime.Scoped);
            }
            else
            {
                services
                    .AddDbContext<TescoBOMContext>(options =>
                    {
                        options.UseSqlServer(configuration.GetConnectionString("TescoBOM"));
                    }, ServiceLifetime.Scoped);
            }

            services.AddScoped<ProjectBOMRepository>();
            services.AddScoped<MasterBOMRepository>();
            services.AddScoped<ProjectPartsRepository>();
            services.AddScoped<ShipingUsersRepository>();

            services.AddScoped<TescoBOMUnitOfWork>();

            return services;
        }
    }
}