﻿using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.Context;
using TescoControls.TescoBOM.Data.Repositories;

namespace TescoControls.TescoBOM.Data.UnitOfWork
{
    public class TescoBOMUnitOfWork
    {

        private readonly TescoBOMContext _dbContext;
        private readonly ILogger _logger;

        public ProjectBOMRepository ProjectBOMRepository { get; }
        public MasterBOMRepository MasterBOMRepository { get; }
        public ProjectPartsRepository ProjectPartsRepository { get; }
        public ShipingUsersRepository ShipingUsersRepository { get; }

        public TescoBOMUnitOfWork(
            TescoBOMContext tescoBOMContext,
            ProjectBOMRepository projectBOMRepository,
            MasterBOMRepository masterBOMRepository,
            ProjectPartsRepository projectPartsRepository,
            ShipingUsersRepository shipingUsersRepository,
            ILogger<TescoBOMUnitOfWork> logger)
        {
            _dbContext = tescoBOMContext;

            ProjectBOMRepository = projectBOMRepository;
            MasterBOMRepository = masterBOMRepository;
            ProjectPartsRepository = projectPartsRepository;
            ShipingUsersRepository = shipingUsersRepository;

            _logger = logger;
        }

        public int SaveChanges() => _dbContext.SaveChanges();

        public IDbContextTransaction BeginTransaction() => _dbContext.Database.BeginTransaction();
    }
}
