﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using TescoControls.TescoBOM.Data.Context;
using TescoControls.TescoBOM.Data.Dtos;
using TescoControls.Utilities.CommonClasses;
using TescoControls.Utilities.Exceptions;

namespace TescoControls.TescoBOM.Data.Repositories
{
    public class MasterBOMRepository : BaseRepository
    {
        public MasterBOMRepository(
            TescoBOMContext tescoBOMContext, 
            ILogger<MasterBOMRepository> logger) 
            : base(tescoBOMContext, logger)
        {
        }

        public PaginatedList<MasterBOMDto> Get(
            string searchPart,
            int offset,
            int pageSize,
            string sortField,
            bool sortOrder)
        {
            var query = _dbContext.TblMasterBom
                .Join(_dbContext.TblMmanufacturerX,
                mb => mb.Manufacturer,
                m => m.ManufacturerId,
                (mb, m) => new MasterBOMDto 
                {
                    Manufacturer = m.Manufacturer,
                    PartNumber = mb.PartNumber,
                    TechData = mb.Techdata,
                    Description = mb.Description
                })
                .Where(mb => mb.Manufacturer.Contains(searchPart))
                .AsNoTracking();

            var count = query.Count();

            if (count == 0)
                throw new NotFoundException(); //todo: add additional data to this constructor

            switch (sortField)
            {
                case "partnumber":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.PartNumber)
                            : query.OrderBy(q => q.PartNumber);
                        break;
                    }
                case "techdata":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.TechData)
                            : query.OrderBy(q => q.TechData);
                        break;
                    }
                case "description":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Description)
                            : query.OrderBy(q => q.Description);
                        break;
                    }
                default:
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Manufacturer)
                            : query.OrderBy(q => q.Manufacturer);
                        break;
                    }
            }

            var records = query
                .Skip(offset)
                .Take(pageSize)
                .ToList();

            var result = new PaginatedList<MasterBOMDto>
            {
                Records = records,
                Count = count
            };

            return result;
        }
    }
}
