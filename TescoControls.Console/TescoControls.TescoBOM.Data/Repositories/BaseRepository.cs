﻿using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.Context;

namespace TescoControls.TescoBOM.Data.Repositories
{
    public class BaseRepository
    {
        protected TescoBOMContext _dbContext;
        protected ILogger _logger;

        protected BaseRepository(
            TescoBOMContext tescoBOMContext,
            ILogger logger)
        {
            _dbContext = tescoBOMContext;
            _logger = logger;
        }
    }
}
