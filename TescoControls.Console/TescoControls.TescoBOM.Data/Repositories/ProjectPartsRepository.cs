﻿using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.Context;

namespace TescoControls.TescoBOM.Data.Repositories
{
    public class ProjectPartsRepository : BaseRepository
    {
        public ProjectPartsRepository(
            TescoBOMContext tescoBOMContext, 
            ILogger<ProjectPartsRepository> logger) 
            : base(tescoBOMContext, logger)
        {
        }
    }
}
