﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.TescoBOM.Data.Context;
using TescoControls.TescoBOM.Data.Dtos;
using TescoControls.Utilities.CommonClasses;
using TescoControls.Utilities.Exceptions;

namespace TescoControls.TescoBOM.Data.Repositories
{
    public class ProjectBOMRepository : BaseRepository
    {
        public ProjectBOMRepository(
            TescoBOMContext tescoBOMContext, 
            ILogger<ProjectBOMRepository> logger) 
            : base(tescoBOMContext, logger)
        {
        }

        public PaginatedList<ProjectBOMDto> Get(
            string searchPart,
            int offset,
            int pageSize,
            string sortField,
            bool sortOrder)
        {
            var query = _dbContext.TblProjectBoms
                .Where(d => d.Bomname.Contains(searchPart))
                .Join(_dbContext.TblProjectMasterBom,
                pb => pb.Projectid,
                pmb => pmb.Projectid,
                (pb, pmb) => new { Project = pb, ProjectMaster = pmb })
                //.Join(_dbContext.TblProjectBom,
                //pb => pb.ProjectMaster.ProjectBomitemId,
                //tpb => tpb.ProjectBomitemId,
                //(pb, tpb) => new { pb.Project, pb.ProjectMaster, ProjectBom = tpb })
                .Join(_dbContext.TblprojectBomSection,
                pb => pb.Project.Bomid,
                pbs => pbs.Bomid,
                (pb, pbs) => new { pb.Project, pb.ProjectMaster, /*pb.ProjectBom,*/ Section = pbs })
                .Join(_dbContext.TblMmanufacturerX,
                pb => pb.ProjectMaster.Manufacturer,
                m => m.ManufacturerId,
                (pb, m) => new { pb.Project, pb.ProjectMaster, /*pb.ProjectBom,*/ pb.Section, m.Manufacturer })
                .Join(_dbContext.TblProjectBomSectionSubsection,
                ps => ps.Section.Sectionid,
                pbss => pbss.Sectionid,
                (ps, pbss) => new ProjectBOMDto
                {
                    BOMName = ps.Project.Bomname,
                    Section = ps.Section.SectionText,
                    Subsection = pbss.SubSectionText,
                    Item = 0, //ps.ProjectBom.ItemNumber,
                    Quantity = 0, //ps.ProjectBom.Quantity,
                    Manufacturer = ps.Manufacturer,
                    PartNumber = ps.ProjectMaster.PartNumber,
                    Tag = "",
                    Symbol = ps.ProjectMaster.SymbRef,
                    Description = ps.ProjectMaster.Description
                })
                .AsNoTracking();

            var count = query.Count();

            if (count == 0)
                throw new NotFoundException(); //todo: add additional data to this constructor

            switch (sortField)
            {
                case "section":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Section)
                            : query.OrderBy(q => q.Section);
                        break;
                    }
                case "subsection":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Subsection)
                            : query.OrderBy(q => q.Subsection);
                        break;
                    }
                case "item":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Item)
                            : query.OrderBy(q => q.Item);
                        break;
                    }
                case "quantity":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Quantity)
                            : query.OrderBy(q => q.Quantity);
                        break;
                    }
                case "manufacturer":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Manufacturer)
                            : query.OrderBy(q => q.Manufacturer);
                        break;
                    }
                case "partnumber":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.PartNumber)
                            : query.OrderBy(q => q.PartNumber);
                        break;
                    }
                case "tag":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Tag)
                            : query.OrderBy(q => q.Tag);
                        break;
                    }
                case "symbol":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Symbol)
                            : query.OrderBy(q => q.Symbol);
                        break;
                    }
                default:
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.BOMName)
                            : query.OrderBy(q => q.BOMName);
                        break;
                    }
            }

            var records = query
                .Skip(offset)
                .Take(pageSize)
                .ToList();

            var result = new PaginatedList<ProjectBOMDto>
            {
                Records = records,
                Count = count
            };

            return result;
        }
    }
}
