﻿using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.Context;

namespace TescoControls.TescoBOM.Data.Repositories
{
    public class ShipingUsersRepository : BaseRepository
    {
        public ShipingUsersRepository(
            TescoBOMContext tescoBOMContext, 
            ILogger<ShipingUsersRepository> logger) 
            : base(tescoBOMContext, logger)
        {
        }
    }
}
