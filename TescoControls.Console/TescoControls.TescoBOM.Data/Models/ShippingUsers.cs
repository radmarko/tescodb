﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class ShippingUsers
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public byte? Dept { get; set; }
    }
}
