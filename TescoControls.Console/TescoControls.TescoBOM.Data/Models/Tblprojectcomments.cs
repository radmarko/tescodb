﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class Tblprojectcomments
    {
        public int? Projectitemid { get; set; }
        public int Projectcommentid { get; set; }
        public string Comment { get; set; }
    }
}
