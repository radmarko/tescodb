﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblprojectBomSection
    {
        public int Sectionid { get; set; }
        public int? Bomid { get; set; }
        public string SectionText { get; set; }
        public int? SectionOrder { get; set; }
        public string SectionLevel1 { get; set; }
        public string SectionLevel2 { get; set; }
        public string SectionLevel3 { get; set; }
    }
}
