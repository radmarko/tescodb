﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblMasterBom1
    {
        public int ItemId { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Techdata { get; set; }
        public int? Manufacturer { get; set; }
        public string Assembly { get; set; }
        public decimal? PriceB { get; set; }
        public decimal? PriceS { get; set; }
        public double? X { get; set; }
        public double? Y { get; set; }
        public double? Z { get; set; }
        public string SymbRef { get; set; }
        public DateTime? DateCr { get; set; }
        public DateTime? DateCh { get; set; }
        public string DevType { get; set; }
        public bool? MasterItem { get; set; }
        public string FromProject { get; set; }
    }
}
