﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblCategoryMinor
    {
        public TblCategoryMinor()
        {
            TblCategorySub = new HashSet<TblCategorySub>();
        }

        public int CatMinor { get; set; }
        public int CatMajorId { get; set; }
        public string CatMinorDesc { get; set; }

        public virtual TblCategoryMajor CatMajor { get; set; }
        public virtual ICollection<TblCategorySub> TblCategorySub { get; set; }
    }
}
