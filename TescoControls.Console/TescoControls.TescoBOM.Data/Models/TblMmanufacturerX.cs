﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblMmanufacturerX
    {
        public int ManufacturerId { get; set; }
        public string Manufacturer { get; set; }
    }
}
