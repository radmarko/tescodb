﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblProjectParts
    {
        public int ProjectPartsId { get; set; }
        public string ProjectNumber { get; set; }
        public int? PrintOrder { get; set; }
        public int? ItemNo { get; set; }
        public string Symbol { get; set; }
        public int? Qty { get; set; }
        public string PartNumber { get; set; }
        public string Units { get; set; }
        public string Manufacturer { get; set; }
        public string Description { get; set; }
        public decimal? UnitPrice { get; set; }
        public string UlOrUrRecognition { get; set; }
        public string PreferredUsage { get; set; }
        public string Category { get; set; }
        public string TescoPartNumber { get; set; }
        public string Flag1 { get; set; }
        public string Flag2 { get; set; }
        public string Flag3 { get; set; }
        public string Flag4 { get; set; }
        public string Tagname { get; set; }
    }
}
