﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblProjectBomSectionSubsection
    {
        public int Subsectionid { get; set; }
        public int? Sectionid { get; set; }
        public string SubSectionText { get; set; }
        public int? ItemNumberStart { get; set; }
        public int? SubsectionOrder { get; set; }
    }
}
