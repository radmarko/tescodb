﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblProjectBoms
    {
        public int Bomid { get; set; }
        public string Projectid { get; set; }
        public string Bomname { get; set; }
        public int? Bomorder { get; set; }
    }
}
