﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblProjectMasterBom
    {
        public string Projectid { get; set; }
        public int ProjectBomitemId { get; set; }
        public int? MasterBomitemId { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Techdata { get; set; }
        public int? Manufacturer { get; set; }
        public string Assembly { get; set; }
        public decimal? PriceB { get; set; }
        public decimal? PriceS { get; set; }
        public string SymbRef { get; set; }
        public DateTime? DateCr { get; set; }
        public DateTime? DateCh { get; set; }
        public string DevType { get; set; }
        public bool? MasterItem { get; set; }
        public string FromProject { get; set; }
    }
}
