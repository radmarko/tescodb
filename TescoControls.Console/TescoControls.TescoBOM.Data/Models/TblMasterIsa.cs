﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblMasterIsa
    {
        public double? ItemNo { get; set; }
        public string InstrumentType { get; set; }
        public string LoopNo { get; set; }
        public string Manufacturer { get; set; }
        public string LoopDescription { get; set; }
        public string ProcessMedium { get; set; }
        public string ProcessConnection { get; set; }
        public string ProcessRange { get; set; }
        public string InstrumentRange { get; set; }
        public string InstrumentSpan { get; set; }
        public string EngineeringUnits { get; set; }
        public string TransmitterTagRefNo { get; set; }
        public string TransmitterModelNo { get; set; }
        public string Enclosure { get; set; }
        public string Mounting { get; set; }
        public string DevicePower { get; set; }
        public string SignalInput { get; set; }
        public string SignalOutput { get; set; }
        public string Display { get; set; }
        public string AuxiliaryInput { get; set; }
        public string AuxiliaryOutput { get; set; }
        public string DiscreteOutputs { get; set; }
        public string OutputTagRef1 { get; set; }
        public string OutputTagRef2 { get; set; }
        public string OutputTagRef3 { get; set; }
        public string SensorTagRefNo { get; set; }
        public string SensorModelNo { get; set; }
        public string PrimaryElement { get; set; }
        public string SensorMaterial { get; set; }
        public string CableLength { get; set; }
        public string Accuracy { get; set; }
        public string MinimumFlow { get; set; }
        public string MinimumTemperature { get; set; }
        public string MinimumPressure { get; set; }
        public string MaximumFlow { get; set; }
        public string MaximumTemperature { get; set; }
        public string MaximumPressure { get; set; }
        public string Option1 { get; set; }
        public string Option2 { get; set; }
        public string Option3 { get; set; }
        public string Option1ModelNo { get; set; }
        public string Option2ModelNo { get; set; }
        public string Option3ModelNo { get; set; }
        public string SpecialNotes { get; set; }
        public string TpNo { get; set; }
        public string SpecificationSubsection { get; set; }
    }
}
