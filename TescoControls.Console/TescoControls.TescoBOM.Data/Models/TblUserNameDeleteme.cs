﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblUserNameDeleteme
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Project { get; set; }
        public string Computername { get; set; }
        public byte? LoggedIn { get; set; }
    }
}
