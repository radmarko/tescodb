﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblMasterBom
    {
        public int MasterBomitemId { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Techdata { get; set; }
        public int? Manufacturer { get; set; }
        public string Assembly { get; set; }
        public decimal? PriceB { get; set; }
        public decimal? PriceS { get; set; }
        public string SymbRef { get; set; }
        public DateTime? DateCr { get; set; }
        public DateTime? DateCh { get; set; }
        public string DevType { get; set; }
        public bool? MasterItem { get; set; }
        public string FromProject { get; set; }
        public string Preferred { get; set; }
        public int? CatMajor { get; set; }
        public int? CatMinor { get; set; }
        public int? CatSub { get; set; }
    }
}
