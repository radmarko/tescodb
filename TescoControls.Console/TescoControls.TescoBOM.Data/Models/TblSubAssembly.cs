﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblSubAssembly
    {
        public int SubItemId { get; set; }
        public int? ProjectItemId { get; set; }
        public int? ProjectNumber { get; set; }
        public int? ItemId { get; set; }
        public double? Quantity { get; set; }
        public string Range { get; set; }
        public string Symbol { get; set; }
        public double? ItemNumber { get; set; }
    }
}
