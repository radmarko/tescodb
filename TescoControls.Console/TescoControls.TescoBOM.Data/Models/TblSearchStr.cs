﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblSearchStr
    {
        public int TableId { get; set; }
        public int? UserId { get; set; }
        public byte? TableIndex { get; set; }
        public byte? FrameOption { get; set; }
        public string SearchStr { get; set; }
    }
}
