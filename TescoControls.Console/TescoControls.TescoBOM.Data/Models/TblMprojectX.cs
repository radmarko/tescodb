﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblMprojectX
    {
        public string ProjectNumber { get; set; }
        public string ProjectName { get; set; }
        public string ProjectLocation { get; set; }
        public string ProjectState { get; set; }
        public string ProjectOwner { get; set; }
        public string ProjectEngr { get; set; }
        public string ProjectCustomer { get; set; }
        public string ProjectPo { get; set; }
        public string RevisionNumber { get; set; }
        public DateTime? RevisionDate { get; set; }
        public DateTime? ProjectDate { get; set; }
        public string LoginName { get; set; }
        public string Computername { get; set; }
        public string UserDefined { get; set; }
        public string ProjectBackUp { get; set; }
        public string ProjectComments { get; set; }
        public string ProjectPslipComments { get; set; }
    }
}
