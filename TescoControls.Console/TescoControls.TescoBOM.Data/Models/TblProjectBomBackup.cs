﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblProjectBomBackup
    {
        public int ProjectItemId { get; set; }
        public string ProjectNumber { get; set; }
        public int? ProjectBomitemId { get; set; }
        public double? Quantity { get; set; }
        public string SpecSection { get; set; }
        public int? Subsectionid { get; set; }
        public string Range { get; set; }
        public string TagRef { get; set; }
        public string Symbol { get; set; }
        public double? ItemNumber { get; set; }
        public int? SubTo { get; set; }
        public int? Itemorder { get; set; }
        public string ShipNow { get; set; }
        public string Shipped { get; set; }
        public DateTime? ShipDate { get; set; }
        public string Received { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public double? QtyShipped { get; set; }
        public double? QtyRemaining { get; set; }
    }
}
