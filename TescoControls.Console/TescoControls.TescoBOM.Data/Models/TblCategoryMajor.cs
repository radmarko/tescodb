﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblCategoryMajor
    {
        public TblCategoryMajor()
        {
            TblCategoryMinor = new HashSet<TblCategoryMinor>();
        }

        public int CatMajor { get; set; }
        public string CatMajorDesc { get; set; }

        public virtual ICollection<TblCategoryMinor> TblCategoryMinor { get; set; }
    }
}
