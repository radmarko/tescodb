﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblTempMaster
    {
        public int TempMasterId { get; set; }
        public int? Manufacturer { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string FromProject { get; set; }
        public DateTime? DateCr { get; set; }
        public int? MasterBomitemId { get; set; }
        public string Techdata { get; set; }
    }
}
