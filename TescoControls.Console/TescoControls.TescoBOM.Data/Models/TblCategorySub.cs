﻿using System;
using System.Collections.Generic;

namespace TescoControls.TescoBOM.Data.Models
{
    public partial class TblCategorySub
    {
        public int CatSub { get; set; }
        public int CatMinorId { get; set; }
        public string CatSubDesc { get; set; }

        public virtual TblCategoryMinor CatMinor { get; set; }
    }
}
