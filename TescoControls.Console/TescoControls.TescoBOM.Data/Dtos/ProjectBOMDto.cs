﻿namespace TescoControls.TescoBOM.Data.Dtos
{
    public class ProjectBOMDto
    {
        public string BOMName { get; set; }
        public string Section { get; set; }
        public string Subsection { get; set; }
        public double? Item { get; set; }
        public double? Quantity { get; set; }
        public string Manufacturer { get; set; }
        public string PartNumber { get; set; }
        public string Tag { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }

    }
}
