﻿namespace TescoControls.TescoBOM.Data.Dtos
{
    public class MasterBOMDto
    {
        public string Manufacturer { get; set; }
        public string PartNumber { get; set; }
        public string TechData { get; set; }
        public string Description { get; set; }
    }
}
