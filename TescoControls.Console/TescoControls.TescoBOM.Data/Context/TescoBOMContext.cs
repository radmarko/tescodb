﻿using Microsoft.EntityFrameworkCore;
using TescoControls.TescoBOM.Data.Models;

namespace TescoControls.TescoBOM.Data.Context
{
    public partial class TescoBOMContext : DbContext
    {
        public TescoBOMContext()
        {
        }

        public TescoBOMContext(DbContextOptions<TescoBOMContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ShippingUsers> ShippingUsers { get; set; }
        public virtual DbSet<TblCategoryMajor> TblCategoryMajor { get; set; }
        public virtual DbSet<TblCategoryMinor> TblCategoryMinor { get; set; }
        public virtual DbSet<TblCategorySub> TblCategorySub { get; set; }
        public virtual DbSet<TblMasterBom> TblMasterBom { get; set; }
        public virtual DbSet<TblMasterBom1> TblMasterBom1 { get; set; }
        public virtual DbSet<TblMasterBomBkup> TblMasterBomBkup { get; set; }
        public virtual DbSet<TblMasterIsa> TblMasterIsa { get; set; }
        public virtual DbSet<TblMmanufacturerX> TblMmanufacturerX { get; set; }
        public virtual DbSet<TblMprojectX> TblMprojectX { get; set; }
        public virtual DbSet<TblProjectBom> TblProjectBom { get; set; }
        public virtual DbSet<TblProjectBomBackup> TblProjectBomBackup { get; set; }
        public virtual DbSet<TblProjectBomSectionSubsection> TblProjectBomSectionSubsection { get; set; }
        public virtual DbSet<TblProjectBoms> TblProjectBoms { get; set; }
        public virtual DbSet<TblProjectMasterBom> TblProjectMasterBom { get; set; }
        public virtual DbSet<TblProjectParts> TblProjectParts { get; set; }
        public virtual DbSet<TblSearchStr> TblSearchStr { get; set; }
        public virtual DbSet<TblSubAssembly> TblSubAssembly { get; set; }
        public virtual DbSet<TblTempMaster> TblTempMaster { get; set; }
        public virtual DbSet<TblUserName> TblUserName { get; set; }
        public virtual DbSet<TblUserNameDeleteme> TblUserNameDeleteme { get; set; }
        public virtual DbSet<TblprojectBomSection> TblprojectBomSection { get; set; }
        public virtual DbSet<Tblprojectcomments> Tblprojectcomments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-MNNOJHC\\SQLEXPRESS;Database=TescoBOM;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ShippingUsers>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.UserId)
                    .HasColumnName("userID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.UserName)
                    .HasColumnName("userName")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCategoryMajor>(entity =>
            {
                entity.HasKey(e => e.CatMajor);

                entity.ToTable("tblCategoryMajor");

                entity.Property(e => e.CatMajor).HasColumnName("catMajor");

                entity.Property(e => e.CatMajorDesc)
                    .IsRequired()
                    .HasColumnName("catMajorDesc")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCategoryMinor>(entity =>
            {
                entity.HasKey(e => e.CatMinor);

                entity.ToTable("tblCategoryMinor");

                entity.Property(e => e.CatMinor).HasColumnName("catMinor");

                entity.Property(e => e.CatMajorId).HasColumnName("catMajorID");

                entity.Property(e => e.CatMinorDesc)
                    .IsRequired()
                    .HasColumnName("catMinorDesc")
                    .HasMaxLength(50);

                entity.HasOne(d => d.CatMajor)
                    .WithMany(p => p.TblCategoryMinor)
                    .HasForeignKey(d => d.CatMajorId)
                    .HasConstraintName("FK_tblCategoryMinor_tblCategoryMajor");
            });

            modelBuilder.Entity<TblCategorySub>(entity =>
            {
                entity.HasKey(e => e.CatSub);

                entity.ToTable("tblCategorySub");

                entity.Property(e => e.CatSub).HasColumnName("catSub");

                entity.Property(e => e.CatMinorId).HasColumnName("catMinorID");

                entity.Property(e => e.CatSubDesc)
                    .IsRequired()
                    .HasColumnName("catSubDesc")
                    .HasMaxLength(50);

                entity.HasOne(d => d.CatMinor)
                    .WithMany(p => p.TblCategorySub)
                    .HasForeignKey(d => d.CatMinorId)
                    .HasConstraintName("FK_tblCategorySub_tblCategoryMinor");
            });

            modelBuilder.Entity<TblMasterBom>(entity =>
            {
                entity.HasKey(e => e.MasterBomitemId)
                    .IsClustered(false);

                entity.ToTable("tblMasterBOM");

                entity.HasIndex(e => e.PartNumber)
                    .HasName("PartNumber")
                    .IsClustered();

                entity.Property(e => e.MasterBomitemId).HasColumnName("MasterBOMItemID");

                entity.Property(e => e.Assembly)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CatMajor).HasColumnName("catMajor");

                entity.Property(e => e.CatMinor).HasColumnName("catMinor");

                entity.Property(e => e.CatSub).HasColumnName("catSub");

                entity.Property(e => e.DateCh)
                    .HasColumnName("Date_Ch")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCr)
                    .HasColumnName("Date_Cr")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DevType)
                    .HasColumnName("Dev_Type")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FromProject)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PartNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Preferred)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PriceB)
                    .HasColumnName("Price_B")
                    .HasColumnType("money");

                entity.Property(e => e.PriceS)
                    .HasColumnName("Price_S")
                    .HasColumnType("money");

                entity.Property(e => e.SymbRef)
                    .HasColumnName("Symb_ref")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Techdata)
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblMasterBom1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblMasterBOM1");

                entity.Property(e => e.Assembly).HasMaxLength(10);

                entity.Property(e => e.DateCh)
                    .HasColumnName("Date_Ch")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCr)
                    .HasColumnName("Date_Cr")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.DevType)
                    .HasColumnName("Dev_Type")
                    .HasMaxLength(15);

                entity.Property(e => e.FromProject).HasMaxLength(10);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.PartNumber).HasMaxLength(50);

                entity.Property(e => e.PriceB)
                    .HasColumnName("Price_B")
                    .HasColumnType("money");

                entity.Property(e => e.PriceS)
                    .HasColumnName("Price_S")
                    .HasColumnType("money");

                entity.Property(e => e.SymbRef)
                    .HasColumnName("Symb_ref")
                    .HasMaxLength(20);

                entity.Property(e => e.Techdata).HasMaxLength(60);
            });

            modelBuilder.Entity<TblMasterBomBkup>(entity =>
            {
                entity.HasKey(e => e.MasterBomitemId)
                    .IsClustered(false);

                entity.ToTable("tblMasterBOM_bkup");

                entity.Property(e => e.MasterBomitemId).HasColumnName("MasterBOMItemID");

                entity.Property(e => e.Assembly)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CatMajor).HasColumnName("catMajor");

                entity.Property(e => e.CatMinor).HasColumnName("catMinor");

                entity.Property(e => e.CatSub).HasColumnName("catSub");

                entity.Property(e => e.DateCh)
                    .HasColumnName("Date_Ch")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCr)
                    .HasColumnName("Date_Cr")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DevType)
                    .HasColumnName("Dev_Type")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FromProject)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PartNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Preferred)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PriceB)
                    .HasColumnName("Price_B")
                    .HasColumnType("money");

                entity.Property(e => e.PriceS)
                    .HasColumnName("Price_S")
                    .HasColumnType("money");

                entity.Property(e => e.SymbRef)
                    .HasColumnName("Symb_ref")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Techdata)
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblMasterIsa>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblMasterISA");

                entity.Property(e => e.Accuracy).HasMaxLength(20);

                entity.Property(e => e.AuxiliaryInput)
                    .HasColumnName("Auxiliary Input")
                    .HasMaxLength(15);

                entity.Property(e => e.AuxiliaryOutput)
                    .HasColumnName("Auxiliary Output")
                    .HasMaxLength(15);

                entity.Property(e => e.CableLength)
                    .HasColumnName("Cable Length")
                    .HasMaxLength(20);

                entity.Property(e => e.DevicePower)
                    .HasColumnName("Device Power")
                    .HasMaxLength(15);

                entity.Property(e => e.DiscreteOutputs)
                    .HasColumnName("Discrete Outputs")
                    .HasMaxLength(15);

                entity.Property(e => e.Display).HasMaxLength(15);

                entity.Property(e => e.Enclosure).HasMaxLength(15);

                entity.Property(e => e.EngineeringUnits)
                    .HasColumnName("Engineering Units")
                    .HasMaxLength(50);

                entity.Property(e => e.InstrumentRange)
                    .HasColumnName("Instrument Range")
                    .HasMaxLength(25);

                entity.Property(e => e.InstrumentSpan)
                    .HasColumnName("Instrument Span")
                    .HasMaxLength(20);

                entity.Property(e => e.InstrumentType)
                    .HasColumnName("Instrument type")
                    .HasMaxLength(45);

                entity.Property(e => e.ItemNo).HasColumnName("ITEM NO#");

                entity.Property(e => e.LoopDescription)
                    .HasColumnName("Loop Description")
                    .HasMaxLength(60);

                entity.Property(e => e.LoopNo)
                    .HasColumnName("Loop No#")
                    .HasMaxLength(10);

                entity.Property(e => e.Manufacturer).HasMaxLength(45);

                entity.Property(e => e.MaximumFlow)
                    .HasColumnName("Maximum Flow")
                    .HasMaxLength(20);

                entity.Property(e => e.MaximumPressure)
                    .HasColumnName("Maximum Pressure")
                    .HasMaxLength(20);

                entity.Property(e => e.MaximumTemperature)
                    .HasColumnName("Maximum Temperature")
                    .HasMaxLength(20);

                entity.Property(e => e.MinimumFlow)
                    .HasColumnName("Minimum Flow")
                    .HasMaxLength(20);

                entity.Property(e => e.MinimumPressure)
                    .HasColumnName("Minimum Pressure")
                    .HasMaxLength(20);

                entity.Property(e => e.MinimumTemperature)
                    .HasColumnName("Minimum Temperature")
                    .HasMaxLength(20);

                entity.Property(e => e.Mounting).HasMaxLength(25);

                entity.Property(e => e.Option1)
                    .HasColumnName("Option 1")
                    .HasMaxLength(45);

                entity.Property(e => e.Option1ModelNo)
                    .HasColumnName("Option 1 Model No#")
                    .HasMaxLength(20);

                entity.Property(e => e.Option2)
                    .HasColumnName("Option 2")
                    .HasMaxLength(45);

                entity.Property(e => e.Option2ModelNo)
                    .HasColumnName("Option 2 Model No#")
                    .HasMaxLength(20);

                entity.Property(e => e.Option3)
                    .HasColumnName("Option 3")
                    .HasMaxLength(45);

                entity.Property(e => e.Option3ModelNo)
                    .HasColumnName("Option 3 Model No#")
                    .HasMaxLength(20);

                entity.Property(e => e.OutputTagRef1)
                    .HasColumnName("Output Tag Ref# 1")
                    .HasMaxLength(10);

                entity.Property(e => e.OutputTagRef2)
                    .HasColumnName("Output Tag Ref# 2")
                    .HasMaxLength(10);

                entity.Property(e => e.OutputTagRef3)
                    .HasColumnName("Output Tag Ref# 3")
                    .HasMaxLength(10);

                entity.Property(e => e.PrimaryElement)
                    .HasColumnName("Primary Element")
                    .HasMaxLength(30);

                entity.Property(e => e.ProcessConnection)
                    .HasColumnName("Process Connection")
                    .HasMaxLength(30);

                entity.Property(e => e.ProcessMedium)
                    .HasColumnName("Process Medium")
                    .HasMaxLength(30);

                entity.Property(e => e.ProcessRange)
                    .HasColumnName("Process Range")
                    .HasMaxLength(25);

                entity.Property(e => e.SensorMaterial)
                    .HasColumnName("Sensor Material")
                    .HasMaxLength(20);

                entity.Property(e => e.SensorModelNo)
                    .HasColumnName("Sensor Model No#")
                    .HasMaxLength(30);

                entity.Property(e => e.SensorTagRefNo)
                    .HasColumnName("Sensor Tag Ref# No#")
                    .HasMaxLength(20);

                entity.Property(e => e.SignalInput)
                    .HasColumnName("Signal Input")
                    .HasMaxLength(15);

                entity.Property(e => e.SignalOutput)
                    .HasColumnName("Signal Output")
                    .HasMaxLength(15);

                entity.Property(e => e.SpecialNotes)
                    .HasColumnName("Special Notes")
                    .HasMaxLength(50);

                entity.Property(e => e.SpecificationSubsection)
                    .HasColumnName("Specification subsection")
                    .HasMaxLength(10);

                entity.Property(e => e.TpNo)
                    .HasColumnName("TP No#")
                    .HasMaxLength(15);

                entity.Property(e => e.TransmitterModelNo)
                    .HasColumnName("Transmitter Model No#")
                    .HasMaxLength(30);

                entity.Property(e => e.TransmitterTagRefNo)
                    .HasColumnName("Transmitter Tag Ref# No#")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<TblMmanufacturerX>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblMManufacturerX");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ManufacturerId)
                    .HasColumnName("ManufacturerID")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<TblMprojectX>(entity =>
            {
                entity.HasKey(e => e.ProjectNumber);

                entity.ToTable("tblMProjectX");

                entity.Property(e => e.ProjectNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Computername)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LoginName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectBackUp)
                    .HasColumnName("ProjectBackUP")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ProjectComments)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectCustomer)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectDate).HasColumnType("datetime");

                entity.Property(e => e.ProjectEngr)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectLocation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectOwner)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectPo)
                    .HasColumnName("ProjectPO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectPslipComments)
                    .HasColumnName("ProjectPSlipComments")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectState)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RevisionDate).HasColumnType("datetime");

                entity.Property(e => e.RevisionNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UserDefined)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblProjectBom>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblProjectBOM");

                entity.HasIndex(e => e.ProjectBomitemId)
                    .HasName("projectBOMItemID")
                    .IsClustered();

                entity.HasIndex(e => new { e.ProjectBomitemId, e.Quantity, e.QtyRemaining, e.ProjectNumber })
                    .HasName("_dta_index_tblProjectBOM_6_946102411__K19_K2_3_4");

                entity.HasIndex(e => new { e.ProjectItemId, e.Subsectionid, e.ProjectBomitemId, e.Itemorder, e.ItemNumber })
                    .HasName("_dta_index_tblProjectBOM_6_946102411__K6_K3_K12_K10_1");

                entity.HasIndex(e => new { e.ShipNow, e.Shipped, e.ShipDate, e.Received, e.Wip, e.Pmgr, e.Subsectionid })
                    .HasName("_dta_index_tblProjectBOM_6_946102411__K6_13_14_15_16_22_23");

                entity.HasIndex(e => new { e.ProjectBomitemId, e.Quantity, e.SpecSection, e.Subsectionid, e.Range, e.TagRef, e.Symbol, e.ItemNumber, e.Itemorder, e.ShipNow, e.Shipped, e.ShipDate, e.Received, e.ReceiveDate, e.QtyShipped, e.QtyRemaining, e.RcvLabel, e.Wip, e.Pmgr, e.ProjectItemId })
                    .HasName("_dta_index_tblProjectBOM_6_946102411__K1_3_4_5_6_7_8_9_10_12_13_14_15_16_17_18_19_20_22_23");

                entity.Property(e => e.Itemorder).HasColumnName("itemorder");

                entity.Property(e => e.Pmgr)
                    .HasColumnName("PMgr")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ProjectBomitemId).HasColumnName("ProjectBOMItemID");

                entity.Property(e => e.ProjectItemId)
                    .HasColumnName("ProjectItemID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ProjectNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Range)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RcvLabel)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ReceiveDate).HasColumnType("datetime");

                entity.Property(e => e.Received)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ShipDate).HasColumnType("datetime");

                entity.Property(e => e.ShipNow)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Shipped)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SpecSection)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Subsectionid).HasColumnName("subsectionid");

                entity.Property(e => e.Symbol)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TagRef)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Wip)
                    .HasColumnName("WIP")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblProjectBomBackup>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblProjectBOM_Backup");

                entity.Property(e => e.Itemorder).HasColumnName("itemorder");

                entity.Property(e => e.ProjectBomitemId).HasColumnName("ProjectBOMItemID");

                entity.Property(e => e.ProjectItemId)
                    .HasColumnName("ProjectItemID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ProjectNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Range)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiveDate).HasColumnType("datetime");

                entity.Property(e => e.Received)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ShipDate).HasColumnType("datetime");

                entity.Property(e => e.ShipNow)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Shipped)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SpecSection)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Subsectionid).HasColumnName("subsectionid");

                entity.Property(e => e.Symbol)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TagRef)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblProjectBomSectionSubsection>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblProjectBomSectionSUBSECTION");

                entity.HasIndex(e => e.Sectionid)
                    .HasName("clusSectionID")
                    .IsClustered();

                entity.HasIndex(e => new { e.Sectionid, e.SubSectionText, e.ItemNumberStart, e.SubsectionOrder, e.Subsectionid })
                    .HasName("_dta_index_tblProjectBomSectionSUBSECTION_6_1701581100__K1_2_3_4_5");

                entity.Property(e => e.ItemNumberStart).HasColumnName("itemNumberStart");

                entity.Property(e => e.Sectionid).HasColumnName("sectionid");

                entity.Property(e => e.SubSectionText)
                    .HasColumnName("subSectionText")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SubsectionOrder).HasColumnName("subsectionOrder");

                entity.Property(e => e.Subsectionid)
                    .HasColumnName("subsectionid")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<TblProjectBoms>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblProjectBOMS");

                entity.HasIndex(e => e.Bomid)
                    .HasName("bomID")
                    .IsClustered();

                entity.Property(e => e.Bomid)
                    .HasColumnName("bomid")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Bomname)
                    .HasColumnName("BOMName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Bomorder).HasColumnName("BOMOrder");

                entity.Property(e => e.Projectid)
                    .HasColumnName("projectid")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblProjectMasterBom>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblProjectMasterBOM");

                entity.HasIndex(e => e.ProjectBomitemId)
                    .HasName("clusProjBOMItemID")
                    .IsClustered();

                entity.Property(e => e.Assembly)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCh)
                    .HasColumnName("Date_Ch")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCr)
                    .HasColumnName("Date_Cr")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DevType)
                    .HasColumnName("Dev_Type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FromProject)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MasterBomitemId).HasColumnName("MasterBOMItemID");

                entity.Property(e => e.PartNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PriceB)
                    .HasColumnName("Price_B")
                    .HasColumnType("money");

                entity.Property(e => e.PriceS)
                    .HasColumnName("Price_S")
                    .HasColumnType("money");

                entity.Property(e => e.ProjectBomitemId)
                    .HasColumnName("ProjectBOMItemID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Projectid)
                    .HasColumnName("projectid")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SymbRef)
                    .HasColumnName("Symb_ref")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Techdata)
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblProjectParts>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblProjectParts");

                entity.HasIndex(e => e.ProjectNumber)
                    .HasName("clusProjNum")
                    .IsClustered();

                entity.HasIndex(e => new { e.ProjectPartsId, e.ProjectNumber })
                    .HasName("idx_projectparts")
                    .IsUnique();

                entity.Property(e => e.Category)
                    .HasColumnName("CATEGORY")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Flag1)
                    .HasColumnName("FLAG 1")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Flag2)
                    .HasColumnName("FLAG 2")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Flag3)
                    .HasColumnName("FLAG 3")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Flag4)
                    .HasColumnName("FLAG 4")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ItemNo).HasColumnName("ITEM NO");

                entity.Property(e => e.Manufacturer)
                    .HasColumnName("MANUFACTURER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PartNumber)
                    .HasColumnName("PART NUMBER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PreferredUsage)
                    .HasColumnName("PREFERRED USAGE")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PrintOrder).HasColumnName("PRINT ORDER");

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("PROJECT NUMBER")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectPartsId)
                    .HasColumnName("PROJECT PARTS ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Qty).HasColumnName("QTY");

                entity.Property(e => e.Symbol)
                    .HasColumnName("SYMBOL")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Tagname)
                    .HasColumnName("TAGNAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TescoPartNumber)
                    .HasColumnName("TESCO PART NUMBER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UlOrUrRecognition)
                    .HasColumnName("UL OR UR RECOGNITION")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("UNIT PRICE")
                    .HasColumnType("money");

                entity.Property(e => e.Units)
                    .HasColumnName("UNITS")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<TblSearchStr>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblSearchStr");

                entity.Property(e => e.FrameOption).HasColumnName("frameOption");

                entity.Property(e => e.SearchStr)
                    .HasColumnName("searchStr")
                    .IsUnicode(false);

                entity.Property(e => e.TableId)
                    .HasColumnName("tableID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.TableIndex).HasColumnName("tableIndex");

                entity.Property(e => e.UserId).HasColumnName("userID");
            });

            modelBuilder.Entity<TblSubAssembly>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblSubAssembly");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ProjectItemId).HasColumnName("ProjectItemID");

                entity.Property(e => e.Range)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SubItemId)
                    .HasColumnName("SubItemID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Symbol)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblTempMaster>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblTempMaster");

                entity.Property(e => e.DateCr)
                    .HasColumnName("date_cr")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FromProject)
                    .HasColumnName("fromProject")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturer).HasColumnName("manufacturer");

                entity.Property(e => e.MasterBomitemId).HasColumnName("MasterBOMItemID");

                entity.Property(e => e.PartNumber)
                    .HasColumnName("partNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Techdata)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.TempMasterId)
                    .HasColumnName("tempMasterID")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<TblUserName>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblUserName");

                entity.HasIndex(e => e.UserName)
                    .HasName("user_name")
                    .IsClustered();

                entity.Property(e => e.Computername)
                    .HasColumnName("computername")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LoggedIn).HasColumnName("loggedIn");

                entity.Property(e => e.Project)
                    .HasColumnName("project")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("userID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.UserName)
                    .HasColumnName("user_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblUserNameDeleteme>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblUserName_deleteme");

                entity.Property(e => e.Computername)
                    .HasColumnName("computername")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LoggedIn).HasColumnName("loggedIn");

                entity.Property(e => e.Project)
                    .HasColumnName("project")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("userID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.UserName)
                    .HasColumnName("user_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblprojectBomSection>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblprojectBomSECTION");

                entity.HasIndex(e => e.Bomid)
                    .HasName("clusBOMID")
                    .IsClustered();

                entity.Property(e => e.Bomid).HasColumnName("BOMID");

                entity.Property(e => e.SectionLevel1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SectionLevel2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SectionLevel3)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SectionOrder).HasColumnName("sectionOrder");

                entity.Property(e => e.SectionText)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Sectionid)
                    .HasColumnName("sectionid")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Tblprojectcomments>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblprojectcomments");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasColumnType("ntext");

                entity.Property(e => e.Projectcommentid)
                    .HasColumnName("projectcommentid")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Projectitemid).HasColumnName("projectitemid");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
