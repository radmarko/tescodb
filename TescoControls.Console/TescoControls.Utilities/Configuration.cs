﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TescoControls.Utilities
{
    public static class Configuration
    {
        public static IConfigurationBuilder AddUtilitiesJsonFiles(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder
                .AddJsonFile("appsettings.utilities.json",
                    optional: false,
                    reloadOnChange: true);

            return configurationBuilder;
        }

        public static IServiceCollection ConfigureUtilitiesServices(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            return services;
        }
    }
}
