﻿using System;

namespace TescoControls.Utilities.Exceptions
{
    public class NotFoundException : Exception
    {

        public NotFoundException() : base()
        { 
        }

        public NotFoundException(string message)
            : base(message)
        {
        }

        public NotFoundException(string item, object key)
            : base(item + " with key: " + key + " is not found!")
        {
        }
    }
}
