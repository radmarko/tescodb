﻿using System.Collections.Generic;

namespace TescoControls.Utilities.CommonClasses
{
    public class PaginatedList<T>
    {
        public List<T> Records { get; set; }
        public int Count { get; set; }
    }
}
