﻿namespace TescoControls.Utilities.CommonClasses
{
    public class Query
    {
        public string Search { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SortField { get; set; }
        public bool SortOrder { get; set; }
    }
}
