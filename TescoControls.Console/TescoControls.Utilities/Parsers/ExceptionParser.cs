﻿using System;

namespace TescoControls.Utilities.Parsers
{
    public static class ExceptionParser
    {
        public static string Parse(Exception e)
        {
            var result = "Message: " + e.Message + "\n"
                + "InnerException: " + e.InnerException + "\n"
                + "StackTrace: " + e.StackTrace;

            return result;
        }
    }
}
