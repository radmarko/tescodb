﻿using System.Collections.Generic;

namespace TescoControls.Helpers.Json
{
    public class PropertyDictionary
    {
        public object Value { get; set; }
        public Dictionary<string, PropertyDictionary> Dictionary { get; set; }
    }
}
