﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TescoControls.TescoBOM.Data;
using TescoControls.TescoBOM.Lib.Services;

namespace TescoControls.TescoBOM.Lib
{
    public static class Configuration
    {
        public static IConfigurationBuilder AddTescoBOMLibJsonFiles(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.AddTescoBOMDataJsonFiles();

            configurationBuilder
                .AddJsonFile("appsettings.tescobom-lib.json",
                    optional: false,
                    reloadOnChange: true);

            return configurationBuilder;
        }

        public static IServiceCollection ConfigureTescoBOMLibServices(
            this IServiceCollection services,
            IConfiguration configuration,
            bool isDevelopment)
        {
            services.ConfigureTescoBOMDataServices(configuration, isDevelopment);

            services.AddScoped<ProjectBOMService>();
            services.AddScoped<MasterBOMService>();
            services.AddScoped<ProjectPartsService>();
            services.AddScoped<ShipingUsersService>();

            return services;
        }
    }
}
