﻿using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.Dtos;
using TescoControls.TescoBOM.Data.UnitOfWork;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.TescoBOM.Lib.Services
{
    public class MasterBOMService : BaseService
    {
        public MasterBOMService(
            TescoBOMUnitOfWork tescoBOMUnitOfWork, 
            ILogger<MasterBOMService> logger) 
            : base(tescoBOMUnitOfWork, logger)
        {
        }

        public PaginatedList<MasterBOMDto> Get(Query search)
        {
            var documents = _tescoBOMUnitOfWork.MasterBOMRepository
                .Get(
                    searchPart: search.Search,
                    offset: (search.Page - 1) * search.PageSize,
                    pageSize: search.PageSize,
                    sortField: search.SortField,
                    sortOrder: search.SortOrder);

            return documents;
        }
    }
}
