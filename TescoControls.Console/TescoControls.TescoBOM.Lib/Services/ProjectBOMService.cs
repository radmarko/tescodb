﻿using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.Dtos;
using TescoControls.TescoBOM.Data.UnitOfWork;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.TescoBOM.Lib.Services
{
    public class ProjectBOMService : BaseService
    {
        public ProjectBOMService(
            TescoBOMUnitOfWork tescoBOMUnitOfWork, 
            ILogger<ProjectBOMService> logger) 
            : base(tescoBOMUnitOfWork, logger)
        {
        }

        public PaginatedList<ProjectBOMDto> Get(Query search)
        {
            var documents = _tescoBOMUnitOfWork.ProjectBOMRepository
                .Get(
                    searchPart: search.Search,
                    offset: (search.Page - 1) * search.PageSize,
                    pageSize: search.PageSize,
                    sortField: search.SortField,
                    sortOrder: search.SortOrder);

            return documents;
        }
    }
}
