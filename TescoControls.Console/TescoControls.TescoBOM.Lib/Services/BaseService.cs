﻿using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.UnitOfWork;

namespace TescoControls.TescoBOM.Lib.Services
{
    public class BaseService
    {
        protected readonly TescoBOMUnitOfWork _tescoBOMUnitOfWork;
        protected readonly ILogger _logger;

        protected BaseService(
            TescoBOMUnitOfWork tescoBOMUnitOfWork, 
            ILogger logger)
        {
            _tescoBOMUnitOfWork = tescoBOMUnitOfWork;
            _logger = logger;
        }
    }
}
