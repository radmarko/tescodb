﻿using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.UnitOfWork;

namespace TescoControls.TescoBOM.Lib.Services
{
    public class ProjectPartsService : BaseService
    {
        public ProjectPartsService(
            TescoBOMUnitOfWork tescoBOMUnitOfWork, 
            ILogger<ProjectPartsService> logger) 
            : base(tescoBOMUnitOfWork, logger)
        {
        }
    }
}
