﻿using Microsoft.Extensions.Logging;
using TescoControls.TescoBOM.Data.UnitOfWork;

namespace TescoControls.TescoBOM.Lib.Services
{
    public class ShipingUsersService : BaseService
    {
        public ShipingUsersService(
            TescoBOMUnitOfWork tescoBOMUnitOfWork, 
            ILogger<ShipingUsersService> logger) 
            : base(tescoBOMUnitOfWork, logger)
        {
        }
    }
}
