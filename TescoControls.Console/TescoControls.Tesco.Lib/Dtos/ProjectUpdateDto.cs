﻿namespace TescoControls.Tesco.Lib.Dtos
{
    public class ProjectUpdateDto
    {
        public string Name { get; set; }
        public string Po { get; set; }
        public string Owner { get; set; }
        public string ProjectState { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
    }
}
