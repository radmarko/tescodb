﻿namespace TescoControls.Tesco.Lib.Dtos
{
    public class DocumentUpdateDto
    {
        public string ProjectNumber { get; set; }
        public int? ContactId { get; set; }
        public int? ContactDocNum { get; set; }
        public int? ProjectDocNum { get; set; }
        public string DocumentType { get; set; }
        public int? DocumentTypeSeqNo { get; set; }
        public string Subject { get; set; }
        public string Author { get; set; }
        public string DocumentStatus { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public decimal? CoAmount { get; set; }
        public int? RfId { get; set; }
    }
}
