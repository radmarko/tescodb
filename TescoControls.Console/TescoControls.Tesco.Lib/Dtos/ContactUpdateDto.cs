﻿using System;

namespace TescoControls.Tesco.Lib.Dtos
{
    public class ContactUpdateDto
    {
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Pager { get; set; }
        public string Title { get; set; }
        public string AlternatePhone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Extension { get; set; }
        public string AlternatePhoneExtension { get; set; }
        public string Department { get; set; }
        public DateTime? EditDate { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string ContactType { get; set; }
        public string AlternateFirstAddress1 { get; set; }
        public string AlternateFirstAddress2 { get; set; }
        public string AlternateFirstCity { get; set; }
        public string AlternateFirstState { get; set; }
        public string AlternateFirstZip { get; set; }
        public string AlternateSecondAddress1 { get; set; }
        public string AlternateSecondAddress2 { get; set; }
        public string AlternateSecondCity { get; set; }
        public string AlternateSecondState { get; set; }
        public string AlternateSecondZip { get; set; }
        public string History { get; set; }
        public string Login { get; set; }
        public bool? TrimaxContact { get; set; }
    }
}
