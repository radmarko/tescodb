﻿using System;
using System.Collections.Generic;
using System.Text;
using TescoControls.Tesco.Data.Models;
using TescoControls.Tesco.Lib.Dtos;

namespace TescoControls.Tesco.Lib.Mappers
{
    public static class DocumentMapper
    {
        public static Documents DocumentCreateDtoToDocument(DocumentCreateDto documentDto)
        {
            var document = new Documents
            {
                ProjectNumber = documentDto.ProjectNumber,
                ContactId = documentDto.ContactId,
                ContactDocnum = documentDto.ContactDocNum,
                ProjectDocnum = documentDto.ProjectDocNum,
                DocumentType = documentDto.DocumentType,
                DocumentTypeSeqno = documentDto.DocumentTypeSeqNo,
                Subject = documentDto.Subject,
                CreateDate = documentDto.CreateDate,
                Author = documentDto.Author,
                DocumentStatus = documentDto.DocumentStatus,
                Docfilename = documentDto.FileName,
                Docfilepath = documentDto.FilePath,
                DocCoAmount = documentDto.CoAmount,
                RfiId = documentDto.RfId,
            };

            return document;
        }

        public static void DocumentUpdateDtoToDocument(DocumentUpdateDto documentDto, Documents document)
        {
            document.ProjectNumber = documentDto.ProjectNumber;
            document.ContactId = documentDto.ContactId;
            document.ContactDocnum = documentDto.ContactDocNum;
            document.ProjectDocnum = documentDto.ProjectDocNum;
            document.DocumentType = documentDto.DocumentType;
            document.DocumentTypeSeqno = documentDto.DocumentTypeSeqNo;
            document.Subject = documentDto.Subject;
            document.Author = documentDto.Author;
            document.DocumentStatus = documentDto.DocumentStatus;
            document.Docfilename = documentDto.FileName;
            document.Docfilepath = documentDto.FilePath;
            document.DocCoAmount = documentDto.CoAmount;
            document.RfiId = documentDto.RfId;
        }
    }
}
