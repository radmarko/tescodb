﻿using TescoControls.Tesco.Data.Models;
using TescoControls.Tesco.Lib.Dtos;

namespace TescoControls.Tesco.Lib.Mappers
{
    public static class ContactMapper
    {
        public static Contacts ContactCreateDtoToContact(ContactCreateDto contactDto)
        {
            var contact = new Contacts
            {
                Company = contactDto.Company,
                Addr1 = contactDto.Address1,
                Addr2 = contactDto.Address2,
                City = contactDto.City,
                State = contactDto.State,
                Zip = contactDto.Zip,
                Phone = contactDto.Phone,
                Fax = contactDto.Fax,
                Homephone = contactDto.HomePhone,
                Mobilephone = contactDto.MobilePhone,
                Pager = contactDto.Pager,
                Title = contactDto.Title,
                Altphone = contactDto.AlternatePhone,
                Fname = contactDto.FirstName,
                Lname = contactDto.LastName,
                Ext = contactDto.Extension,
                AltphoneExt = contactDto.AlternatePhoneExtension,
                Department = contactDto.Department,
                Createdate = contactDto.CreateDate,
                Editdate = null,
                Email = contactDto.Email,
                Url = contactDto.Url,
                ContactType = contactDto.ContactType,
                Alt1Addr1 = contactDto.AlternateFirstAddress1,
                Alt1Addr2 = contactDto.AlternateFirstAddress2,
                Alt1City = contactDto.AlternateFirstCity,
                Alt1State = contactDto.AlternateFirstState,
                Alt1Zip = contactDto.AlternateFirstState,
                Alt2Addr1 = contactDto.AlternateSecondAddress1,
                Alt2Addr2 = contactDto.AlternateSecondAddress2,
                Alt2City = contactDto.AlternateSecondCity,
                Alt2State = contactDto.AlternateSecondState,
                Alt2Zip = contactDto.AlternateSecondZip,
                History = contactDto.History,
                Login = contactDto.Login,
                TrimaxContact = contactDto.TrimaxContact
            };

            return contact;
        }

        public static void ContactUpdateDtoToContact(ContactUpdateDto contactDto, Contacts contact)
        {
            contact.Company = contactDto.Company;
            contact.Addr1 = contactDto.Address1;
            contact.Addr2 = contactDto.Address2;
            contact.City = contactDto.City;
            contact.State = contactDto.State;
            contact.Zip = contactDto.Zip;
            contact.Phone = contactDto.Phone;
            contact.Fax = contactDto.Fax;
            contact.Homephone = contactDto.HomePhone;
            contact.Mobilephone = contactDto.MobilePhone;
            contact.Pager = contactDto.Pager;
            contact.Title = contactDto.Title;
            contact.Altphone = contactDto.AlternatePhone;
            contact.Fname = contactDto.FirstName;
            contact.Lname = contactDto.LastName;
            contact.Ext = contactDto.Extension;
            contact.AltphoneExt = contactDto.AlternatePhoneExtension;
            contact.Department = contactDto.Department;
            contact.Editdate = contactDto.EditDate;
            contact.Email = contactDto.Email;
            contact.Url = contactDto.Url;
            contact.ContactType = contactDto.ContactType;
            contact.Alt1Addr1 = contactDto.AlternateFirstAddress1;
            contact.Alt1Addr2 = contactDto.AlternateFirstAddress2;
            contact.Alt1City = contactDto.AlternateFirstCity;
            contact.Alt1State = contactDto.AlternateFirstState;
            contact.Alt1Zip = contactDto.AlternateFirstState;
            contact.Alt2Addr1 = contactDto.AlternateSecondAddress1;
            contact.Alt2Addr2 = contactDto.AlternateSecondAddress2;
            contact.Alt2City = contactDto.AlternateSecondCity;
            contact.Alt2State = contactDto.AlternateSecondState;
            contact.Alt2Zip = contactDto.AlternateSecondZip;
            contact.History = contactDto.History;
            contact.Login = contactDto.Login;
            contact.TrimaxContact = contactDto.TrimaxContact;
        }
    }
}
