﻿using TescoControls.Tesco.Data.Models;
using TescoControls.Tesco.Lib.Dtos;

namespace TescoControls.Tesco.Lib.Mappers
{
    public static class ProjectMapper
    {
        public static Projects ProjectCreateDtoToProject(ProjectCreateDto projectDto)
        {
            var project = new Projects
            {
                ProjectNumber = projectDto.Number,
                ProjectName = projectDto.Name,
                ProjectPo = projectDto.Po,
                ProjectOwner = projectDto.Owner,
                ProjectState = projectDto.State,
                PrjAddr1 = projectDto.Address1,
                PrjAddr2 = projectDto.Address2,
                PrjCity = projectDto.City,
                PrjState = projectDto.State,
                PrjZip = projectDto.Zip,
                PrjPhone = projectDto.Phone
            };

            return project;
        }

        public static void ProjectUpdateDtoToProject(ProjectUpdateDto projectDto, Projects project)
        {
            project.ProjectName = projectDto.Name;
            project.ProjectPo = projectDto.Po;
            project.ProjectOwner = projectDto.Owner;
            project.ProjectState = projectDto.State;
            project.PrjAddr1 = projectDto.Address1;
            project.PrjAddr2 = projectDto.Address2;
            project.PrjCity = projectDto.City;
            project.PrjState = projectDto.State;
            project.PrjZip = projectDto.Zip;
            project.PrjPhone = projectDto.Phone;
        }
    }
}
