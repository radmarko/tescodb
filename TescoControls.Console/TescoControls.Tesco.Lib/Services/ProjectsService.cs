﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Data.UnitOfWork;
using TescoControls.Tesco.Lib.Dtos;
using TescoControls.Tesco.Lib.Mappers;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Tesco.Lib.Services
{
    public class ProjectsService : BaseService
    {
        public ProjectsService(
            TescoUnitOfWork tescoUnitOfWork, 
            ILogger<ProjectsService> logger) 
            : base(tescoUnitOfWork, logger)
        {
        }

        public async Task<PaginatedList<ProjectDto>> GetAsync(Query search)
        {
            var projects = await _tescoUnitOfWork.ProjectsRepository
                .GetAsync(
                    searchPart: search.Search,
                    offset: (search.Page - 1) * search.PageSize,
                    pageSize: search.PageSize,
                    sortField: search.SortField,
                    sortOrder: search.SortOrder);

            return projects;
        }

        public async Task<ProjectDto> GetAsync(string number)
        {
            var document = await _tescoUnitOfWork.ProjectsRepository
                .GetAsync(number);

            return document;
        }

        public async Task<bool> CreateAsync(ProjectCreateDto projectDto)
        {
            var project = ProjectMapper.ProjectCreateDtoToProject(projectDto);

            await _tescoUnitOfWork.ProjectsRepository.CreateAsync(project);

            return true;
        }

        public async Task<bool> UpdateAsync(string number, ProjectUpdateDto projectDto)
        {
            var project = await _tescoUnitOfWork.ProjectsRepository
                .GetProjectAsync(number);

            ProjectMapper.ProjectUpdateDtoToProject(projectDto, project);
            _tescoUnitOfWork.ProjectsRepository.Update(project);

            return true;
        }

        public async Task<bool> DeleteAsync(string number)
        {
            var project = await _tescoUnitOfWork.ProjectsRepository
                .GetProjectAsync(number);

            _tescoUnitOfWork.ProjectsRepository.Delete(project);

            return true;
        }
    }
}
