﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Data.UnitOfWork;
using TescoControls.Tesco.Lib.Dtos;
using TescoControls.Tesco.Lib.Mappers;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Tesco.Lib.Services
{
    public class DocumentsService : BaseService
    {
        public DocumentsService(
            TescoUnitOfWork tescoUnitOfWork, 
            ILogger<DocumentsService> logger) 
            : base(tescoUnitOfWork, logger)
        {
        }

        public async Task<PaginatedList<DocumentDto>> GetAsync(Query search)
        {
            var documents = await _tescoUnitOfWork.DocumentsRepository
                .GetAsync(
                    searchPart: search.Search,
                    offset: (search.Page - 1) * search.PageSize,
                    pageSize: search.PageSize,
                    sortField: search.SortField,
                    sortOrder: search.SortOrder);

            return documents;
        }

        public PaginatedList<DocumentDto> Get(Query search)
        {
            var documents = _tescoUnitOfWork.DocumentsRepository
                .Get(
                    searchPart: search.Search,
                    offset: (search.Page - 1) * search.PageSize,
                    pageSize: search.PageSize,
                    sortField: search.SortField,
                    sortOrder: search.SortOrder);

            return documents;
        }

        public async Task<DocumentDto> GetAsync(int id)
        {
            var document = await _tescoUnitOfWork.DocumentsRepository
                .GetAsync(id);

            return document;
        }

        public async Task<bool> CreateAsync(DocumentCreateDto documentDto)
        {
            var document = DocumentMapper.DocumentCreateDtoToDocument(documentDto);

            await _tescoUnitOfWork.DocumentsRepository.CreateAsync(document);

            return true;
        }

        public async Task<bool> UpdateAsync(int id, DocumentUpdateDto documentDto)
        {
            var document = await _tescoUnitOfWork.DocumentsRepository
                .GetDocumentAsync(id);

            DocumentMapper.DocumentUpdateDtoToDocument(documentDto, document);
            _tescoUnitOfWork.DocumentsRepository.Update(document);

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var document = await _tescoUnitOfWork.DocumentsRepository
                .GetDocumentAsync(id);

            _tescoUnitOfWork.DocumentsRepository.Delete(document);

            return true;
        }
    }
}
