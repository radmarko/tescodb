﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Data.UnitOfWork;
using TescoControls.Tesco.Lib.Dtos;
using TescoControls.Tesco.Lib.Mappers;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Tesco.Lib.Services
{
    public class ContactsService : BaseService
    {
        public ContactsService(
            TescoUnitOfWork tescoUnitOfWork, 
            ILogger<ContactsService> logger) 
            : base(tescoUnitOfWork, logger)
        {
        }

        public async Task<PaginatedList<ContactGetDto>> GetAsync(Query query)
        {
            var contacts = await _tescoUnitOfWork.ContactsRepository
                .GetAsync(
                    searchPart: query.Search,
                    offset: (query.Page - 1) * query.PageSize,
                    pageSize: query.PageSize,
                    sortField: query.SortField,
                    sortOrder: query.SortOrder);

            return contacts;
        }

        public async Task<ContactDto> GetAsync(int id)
        {
            var company = await _tescoUnitOfWork.ContactsRepository
                .GetAsync(id);

            return company;
        }

        public async Task<bool> CreateAsync(ContactCreateDto contactDto)
        {
            var contact = ContactMapper.ContactCreateDtoToContact(contactDto);
            await _tescoUnitOfWork.ContactsRepository.CreateAsync(contact);

            return true;
        }

        public async Task<bool> UpdateAsync(int id, ContactUpdateDto contactDto)
        {
            var contact = await _tescoUnitOfWork.ContactsRepository
                .GetContactAsync(id);

            ContactMapper.ContactUpdateDtoToContact(contactDto, contact);
            _tescoUnitOfWork.ContactsRepository.Update(contact);

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var contact = await _tescoUnitOfWork.ContactsRepository
                .GetContactAsync(id);

            _tescoUnitOfWork.ContactsRepository.Delete(contact);

            return true;
        }
    }
}
