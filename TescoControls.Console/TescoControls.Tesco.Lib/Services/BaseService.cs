﻿using Microsoft.Extensions.Logging;
using TescoControls.Tesco.Data.UnitOfWork;

namespace TescoControls.Tesco.Lib.Services
{
    public class BaseService
    {
        protected readonly TescoUnitOfWork _tescoUnitOfWork;
        protected readonly ILogger _logger;

        protected BaseService(
            TescoUnitOfWork tescoUnitOfWork,
            ILogger logger)
        {
            _tescoUnitOfWork = tescoUnitOfWork;
            _logger = logger;
        }
    }
}
