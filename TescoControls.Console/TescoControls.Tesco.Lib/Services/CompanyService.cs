﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Data.Models;
using TescoControls.Tesco.Data.UnitOfWork;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Tesco.Lib.Services
{
    public class CompanyService : BaseService
    {
        public CompanyService(
            TescoUnitOfWork tescoUnitOfWork, 
            ILogger<CompanyService> logger) 
            : base(tescoUnitOfWork, logger)
        {
        }

        public async Task<PaginatedList<CompanyDto>> GetAsync(Query query)
        {
            var companies = await _tescoUnitOfWork.CompanyRepository
                .GetAsync(
                    searchPart: query.Search,
                    offset: (query.Page - 1) * query.PageSize,
                    pageSize: query.PageSize,
                    sortField: query.SortField,
                    sortOrder: query.SortOrder);

            return companies;
        }

        public PaginatedList<CompanyDto> Get(Query query)
        {
            var companies = _tescoUnitOfWork.CompanyRepository
                .Get(
                    searchPart: query.Search,
                    offset: (query.Page - 1) * query.PageSize,
                    pageSize: query.PageSize,
                    sortField: query.SortField,
                    sortOrder: query.SortOrder);

            return companies;
        }

        public async Task<CompanyDto> GetAsync(int id)
        {
            var company = await _tescoUnitOfWork.CompanyRepository
                .GetAsync(id);

            return company;
        }

        public async Task<bool> CreateAsync(string name)
        {
            var company = new Company { Company1 = name };
            await _tescoUnitOfWork.CompanyRepository.CreateAsync(company);

            return true;
        }

        public async Task<bool> UpdateAsync(int id, string name)
        {
            var company = await _tescoUnitOfWork.CompanyRepository
                .GetCompanyAsync(id);

            company.Company1 = name;
            _tescoUnitOfWork.CompanyRepository.Update(company);

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var company = await _tescoUnitOfWork.CompanyRepository
                .GetCompanyAsync(id);

            _tescoUnitOfWork.CompanyRepository.Delete(company);

            return true;
        }
    }
}
