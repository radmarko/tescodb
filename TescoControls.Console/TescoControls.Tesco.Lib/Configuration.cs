﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TescoControls.Tesco.Data;
using TescoControls.Tesco.Lib.Services;

namespace TescoControls.Tesco.Lib
{
    public static class Configuration
    {
        public static IConfigurationBuilder AddTescoLibJsonFiles(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.AddTescoDataJsonFiles();

            //configurationBuilder
            //    .AddJsonFile("appsettings.tesco-lib.json",
            //        optional: false,
            //        reloadOnChange: true);

            return configurationBuilder;
        }

        public static IServiceCollection ConfigureTescoLibServices(
            this IServiceCollection services,
            IConfiguration configuration,
            bool isDevelopment)
        {
            services.ConfigureTescoDataServices(configuration, isDevelopment);

            services.AddScoped<CompanyService>();
            services.AddScoped<ContactsService>();
            services.AddScoped<DocumentsService>();
            services.AddScoped<ProjectsService>();

            return services;
        }
    }
}
