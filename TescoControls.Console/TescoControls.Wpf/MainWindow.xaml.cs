﻿using System.Windows;
using TescoControls.Tesco.Lib.Services;
using TescoControls.TescoBOM.Lib.Services;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly DocumentsService _documentsService;
        private readonly ProjectBOMService _projectBOMService;
        private readonly MasterBOMService _masterBOMService;

        public MainWindow(
            DocumentsService documentsService,
            ProjectBOMService projectBOMService,
            MasterBOMService masterBOMService)
        {
            _documentsService = documentsService;
            _projectBOMService = projectBOMService;
            _masterBOMService = masterBOMService;

            InitializeComponent();
            GetCompanies();
        }
        private void GetCompanies()
        {
            var query = new Query
            {
                Search = "",
                Page = 1,
                PageSize = 20,
                SortField = "",
                SortOrder = false
            };

            Document.ItemsSource = _documentsService.Get(query).Records;
            ProjectBOM.ItemsSource = _projectBOMService.Get(query).Records;
            MasterBOM.ItemsSource = _masterBOMService.Get(query).Records;
        }

        private void TabControl_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }
    }
}
