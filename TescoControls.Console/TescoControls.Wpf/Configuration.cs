﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TescoControls.Tesco.Lib;
using TescoControls.TescoBOM.Lib;

namespace TescoControls.Wpf
{
    public static class Configuration
    {
        public static IConfigurationBuilder AddWpfJsonFiles(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.AddTescoLibJsonFiles();
            configurationBuilder.AddTescoBOMLibJsonFiles();

            return configurationBuilder;
        }

        public static IServiceCollection ConfigureWpfServices(
            this IServiceCollection services,
            IConfiguration configuration,
            bool isDevelopment)
        {
            services.ConfigureTescoLibServices(configuration, isDevelopment);
            services.ConfigureTescoBOMLibServices(configuration, isDevelopment);

            services.AddLogging();

            services.AddScoped<MainWindow>();

            return services;
        }
    }
}
