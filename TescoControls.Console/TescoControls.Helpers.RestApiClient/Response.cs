﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TescoControls.Helpers.RestApiClient
{
    public class Response
    {
        public int HttpCode { get; set; }
        public int MyProperty { get; set; }
    }
}
