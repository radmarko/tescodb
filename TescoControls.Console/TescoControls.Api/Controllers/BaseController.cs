﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TescoControls.Api.Controllers
{
    public class BaseController : ControllerBase
    {
        protected readonly ILogger _logger;

        protected BaseController(ILogger logger)
        {
            _logger = logger;
        }
    }
}
