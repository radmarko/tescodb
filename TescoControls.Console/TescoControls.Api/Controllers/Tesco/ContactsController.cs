﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TescoControls.Api.Mappers;
using TescoControls.Api.Mappers.Tesco;
using TescoControls.Api.Models;
using TescoControls.Api.Models.Tesco;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Lib.Services;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Api.Controllers.Tesco
{
    [Route("api/tesco/contacts")]
    [ApiController]
    public class ContactsController : BaseController
    {
        private readonly ContactsService _contactsService;

        public ContactsController(
            ContactsService contactsService,
            ILogger<ContactsController> logger)
            : base(logger)
        {
            _contactsService = contactsService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody] ContactCreateUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                var contactCreate = ContactMapper.ContactCreateUpdateModelToContactCreateDto(model);
                var result = await _contactsService.CreateAsync(contactCreate);

                return Ok(result);
            }

            return BadRequest(
                ModelState.Values.Where(m => m.Errors.Count > 0)
                    .SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ContactGetDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.NotFound)] //todo: add error info
        public async Task<IActionResult> Get(int id)
        {
            var result = await _contactsService.GetAsync(id);

            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<ContactDto>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.NotFound)] //todo: add error info
        [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Get([FromQuery] QueryModel model)
        {
            if (ModelState.IsValid)
            {
                var query = BaseMapper.QueryModelToQuery(model);

                var result = await _contactsService.GetAsync(query);

                return Ok(result);
            }

            return BadRequest(
                ModelState.Values.Where(m => m.Errors.Count > 0)
                    .SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage));
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.NotFound)] //todo: add error info
        [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update(int id, [FromBody] ContactCreateUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                var contactUpdate = ContactMapper.ContactCreateUpdateModelToContactUpdateDto(model);
                var result = await _contactsService.UpdateAsync(id, contactUpdate);

                return Ok(result);
            }

            return BadRequest(
                ModelState.Values.Where(m => m.Errors.Count > 0)
                    .SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.NotFound)] //todo: add error info
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _contactsService.DeleteAsync(id);

            return Ok(result);
        }
    }
}
