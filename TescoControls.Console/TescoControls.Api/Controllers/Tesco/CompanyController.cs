﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TescoControls.Api.Mappers;
using TescoControls.Api.Models;
using TescoControls.Api.Models.Tesco;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Lib.Services;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Api.Controllers.Tesco
{
    [Route("api/tesco/companies")]
    [ApiController]
    public class CompanyController : BaseController
    {
        private readonly CompanyService _companyService;

        public CompanyController(
            CompanyService companyService,
            ILogger<CompanyController> logger)
            : base(logger)
        {
            _companyService = companyService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody] CompanyCreateUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _companyService.CreateAsync(model.Name);

                return Ok(result);
            }

            return BadRequest(
                ModelState.Values.Where(m => m.Errors.Count > 0)
                    .SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CompanyDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.NotFound)] //todo: add error info
        public async Task<IActionResult> Get(int id)
        {
            var result = await _companyService.GetAsync(id);

            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedList<CompanyDto>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.NotFound)] //todo: add error info
        [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Get([FromQuery] QueryModel model)
        {
            if (ModelState.IsValid)
            {
                var query = BaseMapper.QueryModelToQuery(model);

                var result = await _companyService.GetAsync(query);

                return Ok(result);
            }

            return BadRequest(
                ModelState.Values.Where(m => m.Errors.Count > 0)
                    .SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage));
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.NotFound)] //todo: add error info
        [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update(int id, [FromBody] CompanyCreateUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _companyService.UpdateAsync(id, model.Name);

                return Ok(result);
            }

            return BadRequest(
                ModelState.Values.Where(m => m.Errors.Count > 0)
                    .SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.NotFound)] //todo: add error info
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _companyService.DeleteAsync(id);

            return Ok(result);
        }
    }
}
