﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TescoControls.Api.Models.Tesco;
using TescoControls.Tesco.Lib.Dtos;

namespace TescoControls.Api.Mappers.Tesco
{
    public static class ContactMapper
    {
        public static ContactCreateDto ContactCreateUpdateModelToContactCreateDto(ContactCreateUpdateModel model)
        {
            var dto = new ContactCreateDto
            {
                Company = model.Company,
                Address1 = model.Address1,
                Address2 = model.Address2,
                City = model.City,
                State = model.State,
                Zip = model.Zip,
                Phone = model.Phone,
                Fax = model.Fax,
                HomePhone = model.HomePhone,
                MobilePhone = model.MobilePhone,
                Pager = model.Pager,
                Title = model.Title,
                AlternatePhone = model.AlternatePhone,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Extension = model.Extension,
                AlternatePhoneExtension = model.AlternatePhoneExtension,
                Department = model.Department,
                CreateDate = DateTime.Now,
                Email = model.Email,
                Url = model.Url,
                ContactType = model.ContactType,
                AlternateFirstAddress1 = model.AlternateFirstAddress1,
                AlternateFirstAddress2 = model.AlternateFirstAddress2,
                AlternateFirstCity = model.AlternateFirstCity,
                AlternateFirstState = model.AlternateFirstState,
                AlternateFirstZip = model.AlternateFirstZip,
                AlternateSecondAddress1 = model.AlternateSecondAddress1,
                AlternateSecondAddress2 = model.AlternateSecondAddress2,
                AlternateSecondCity = model.AlternateSecondCity,
                AlternateSecondState = model.AlternateSecondState,
                AlternateSecondZip = model.AlternateSecondZip,
                History = model.History,
                Login = model.Login,
                TrimaxContact = model.TrimaxContact
            };

            return dto;
        }

        public static ContactUpdateDto ContactCreateUpdateModelToContactUpdateDto(ContactCreateUpdateModel model)
        {
            var dto = new ContactUpdateDto
            {
                Company = model.Company,
                Address1 = model.Address1,
                Address2 = model.Address2,
                City = model.City,
                State = model.State,
                Zip = model.Zip,
                Phone = model.Phone,
                Fax = model.Fax,
                HomePhone = model.HomePhone,
                MobilePhone = model.MobilePhone,
                Pager = model.Pager,
                Title = model.Title,
                AlternatePhone = model.AlternatePhone,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Extension = model.Extension,
                AlternatePhoneExtension = model.AlternatePhoneExtension,
                Department = model.Department,
                EditDate = DateTime.Now,
                Email = model.Email,
                Url = model.Url,
                ContactType = model.ContactType,
                AlternateFirstAddress1 = model.AlternateFirstAddress1,
                AlternateFirstAddress2 = model.AlternateFirstAddress2,
                AlternateFirstCity = model.AlternateFirstCity,
                AlternateFirstState = model.AlternateFirstState,
                AlternateFirstZip = model.AlternateFirstZip,
                AlternateSecondAddress1 = model.AlternateSecondAddress1,
                AlternateSecondAddress2 = model.AlternateSecondAddress2,
                AlternateSecondCity = model.AlternateSecondCity,
                AlternateSecondState = model.AlternateSecondState,
                AlternateSecondZip = model.AlternateSecondZip,
                History = model.History,
                Login = model.Login,
                TrimaxContact = model.TrimaxContact
            };

            return dto;
        }
    }
}
