﻿using TescoControls.Api.Models;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Api.Mappers
{
    public static class BaseMapper
    {
        //todo: put this in configuration
        private const int DEFAULT_PAGE = 1;
        private const int DEFAULT_PAGE_SIZE = 10;
        private const bool DEFAULT_SORT_ORDER = false;
        private const string DEFAULT_SORT_FIELD = "default";

        public static Query QueryModelToQuery(QueryModel model)
        {
            var query = new Query
            {
                SortOrder = model.SortOrder != null ? (bool)model.SortOrder : DEFAULT_SORT_ORDER,
                SortField = model.SortField != null ? model.SortField : DEFAULT_SORT_FIELD,
                Page = model.Page != null ? (int)model.Page : DEFAULT_PAGE,
                PageSize = model.PageSize != null ? (int)model.PageSize : DEFAULT_PAGE_SIZE,
                Search = model.Search
            };

            return query;
        }
    }
}
