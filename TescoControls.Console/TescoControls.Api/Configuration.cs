﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TescoControls.Api.Middleware;
using TescoControls.Tesco.Lib;

namespace TescoControls.Api
{
    public static class Configuration
    {
        public static IConfigurationBuilder AddApiJsonFiles(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.AddTescoLibJsonFiles();

            configurationBuilder
                .AddJsonFile("appsettings.json",
                    optional: false,
                    reloadOnChange: true);

            return configurationBuilder;
        }

        public static IServiceCollection ConfigureApiServices(
            this IServiceCollection services,
            IConfiguration configuration,
            bool isDevelopment)
        {
            services.ConfigureTescoLibServices(configuration, isDevelopment);

            services.AddSwaggerGen(doc =>
            {
                doc.SwaggerDoc("v1", new OpenApiInfo { Title = "Tesco Controls REST Api", Description = "Tesco Controls Swagger" });

                //doc.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                //{
                //    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                //    Name = "Authorization",
                //    In = ParameterLocation.Header,
                //    Type = SecuritySchemeType.ApiKey
                //});

                //doc.AddSecurityRequirement(new OpenApiSecurityRequirement
                //{
                //    {
                //        new OpenApiSecurityScheme
                //        {
                //            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
                //        },
                //        new string[] { }
                //    }
                //});
            });

            services.AddControllers();

            return services;
        }

        public static void ConfigureApi(
            this IApplicationBuilder app,
            IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(endpoint =>
            {
                endpoint.SwaggerEndpoint("/swagger/v1/swagger.json", "Tesco Controls REST Api");
            });

            app.UseRouting();
            
            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
