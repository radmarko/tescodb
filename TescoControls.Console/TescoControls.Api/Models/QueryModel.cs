﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TescoControls.Api.Models
{
    public class QueryModel
    {
        public string Search { get; set; }
        public string SortField { get; set; }
        public bool? SortOrder { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int? Page { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int? PageSize { get; set; }
    }
}
