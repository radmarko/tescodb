﻿using System.ComponentModel.DataAnnotations;

namespace TescoControls.Api.Models.Tesco
{
    public class CompanyCreateUpdateModel
    {
        [Required]
        public string Name { get; set; }
    }
}
