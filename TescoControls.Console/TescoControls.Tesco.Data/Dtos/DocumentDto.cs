﻿using System;

namespace TescoControls.Tesco.Data.Dtos
{
    public class DocumentDto
    {
        public int Id { get; set; }
        public string ProjectNumber { get; set; }
        public string DocumentType { get; set; }
        public string Subject { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Author { get; set; }
        public string DocumentStatus { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
