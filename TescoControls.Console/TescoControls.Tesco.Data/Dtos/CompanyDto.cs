﻿namespace TescoControls.Tesco.Data.Dtos
{
    public class CompanyDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
