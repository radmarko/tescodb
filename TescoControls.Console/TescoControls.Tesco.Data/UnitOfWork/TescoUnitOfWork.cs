﻿using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using TescoControls.Tesco.Data.Context;
using TescoControls.Tesco.Data.Repositories;

namespace TescoControls.Tesco.Data.UnitOfWork
{
    public class TescoUnitOfWork
    {
        private readonly TescoContext _dbContext;
        private readonly ILogger _logger;

        public CompanyRepository CompanyRepository { get; }
        public ContactsRepository ContactsRepository { get; }
        public DocumentsRepository DocumentsRepository { get; }
        public ProjectsRepository ProjectsRepository { get; }

        public TescoUnitOfWork(
            TescoContext tescoContext,
            CompanyRepository companyRepository,
            ContactsRepository contactsRepository,
            DocumentsRepository documentsRepository,
            ProjectsRepository projectsRepository,
            ILogger<TescoUnitOfWork> logger)
        {
            _dbContext = tescoContext;

            CompanyRepository = companyRepository;
            ContactsRepository = contactsRepository;
            DocumentsRepository = documentsRepository;
            ProjectsRepository = projectsRepository;

            _logger = logger;
        }

        public int SaveChanges() => _dbContext.SaveChanges();

        public IDbContextTransaction BeginTransaction() => _dbContext.Database.BeginTransaction();
    }
}
