﻿using Microsoft.EntityFrameworkCore;
using TescoControls.Tesco.Data.Models;

namespace TescoControls.Tesco.Data.Context
{
    public partial class TescoContext : DbContext
    {
        public TescoContext()
        {
        }

        public TescoContext(DbContextOptions<TescoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<ContactGroups> ContactGroups { get; set; }
        public virtual DbSet<ContactNotes> ContactNotes { get; set; }
        public virtual DbSet<Contacts> Contacts { get; set; }
        public virtual DbSet<DefaultPaths> DefaultPaths { get; set; }
        public virtual DbSet<DepartmentSps> DepartmentSps { get; set; }
        public virtual DbSet<Departments> Departments { get; set; }
        public virtual DbSet<DocumentCategories> DocumentCategories { get; set; }
        public virtual DbSet<DocumentTypes> DocumentTypes { get; set; }
        public virtual DbSet<Documents> Documents { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<InfocenterPrjcontacts> InfocenterPrjcontacts { get; set; }
        public virtual DbSet<MsProjectsSearch> MsProjectsSearch { get; set; }
        public virtual DbSet<OtherInformation> OtherInformation { get; set; }
        public virtual DbSet<PlcTypes> PlcTypes { get; set; }
        public virtual DbSet<ProductView> ProductView { get; set; }
        public virtual DbSet<ProjectContactHistory> ProjectContactHistory { get; set; }
        public virtual DbSet<ProjectContactTypes> ProjectContactTypes { get; set; }
        public virtual DbSet<ProjectContacts> ProjectContacts { get; set; }
        public virtual DbSet<ProjectContactsOld> ProjectContactsOld { get; set; }
        public virtual DbSet<ProjectEventTypes> ProjectEventTypes { get; set; }
        public virtual DbSet<ProjectEvents> ProjectEvents { get; set; }
        public virtual DbSet<ProjectNotes> ProjectNotes { get; set; }
        public virtual DbSet<ProjectOm> ProjectOm { get; set; }
        public virtual DbSet<ProjectParts> ProjectParts { get; set; }
        public virtual DbSet<ProjectPhases> ProjectPhases { get; set; }
        public virtual DbSet<ProjectSecurity> ProjectSecurity { get; set; }
        public virtual DbSet<ProjectStatus> ProjectStatus { get; set; }
        public virtual DbSet<ProjectTypes> ProjectTypes { get; set; }
        public virtual DbSet<Projects> Projects { get; set; }
        public virtual DbSet<Rfi> Rfi { get; set; }
        public virtual DbSet<ScadaTypes> ScadaTypes { get; set; }
        public virtual DbSet<TblMasterBom1> TblMasterBom1 { get; set; }
        public virtual DbSet<TblProjectTeam> TblProjectTeam { get; set; }
        public virtual DbSet<TblVendors> TblVendors { get; set; }
        public virtual DbSet<Templates> Templates { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<VendorGroups> VendorGroups { get; set; }
        public virtual DbSet<VendorNotes> VendorNotes { get; set; }
        public virtual DbSet<View1> View1 { get; set; }
        public virtual DbSet<View2> View2 { get; set; }
        public virtual DbSet<ViewContactGroups> ViewContactGroups { get; set; }
        public virtual DbSet<VwActiveNonPedProjectEvents> VwActiveNonPedProjectEvents { get; set; }
        public virtual DbSet<VwActiveNonPedProjects> VwActiveNonPedProjects { get; set; }
        public virtual DbSet<VwActiveNonPedProjectsParts> VwActiveNonPedProjectsParts { get; set; }
        public virtual DbSet<VwActiveNonPedProjectsWithTeam> VwActiveNonPedProjectsWithTeam { get; set; }
        public virtual DbSet<VwJohnCompanyHistory> VwJohnCompanyHistory { get; set; }
        public virtual DbSet<VwJohnCompanyHistoryLatest> VwJohnCompanyHistoryLatest { get; set; }
        public virtual DbSet<VwJohnCompanyTotalProjects> VwJohnCompanyTotalProjects { get; set; }
        public virtual DbSet<VwJohnCurrentJobs> VwJohnCurrentJobs { get; set; }
        public virtual DbSet<VwJohnCustomerActiveLastHistory> VwJohnCustomerActiveLastHistory { get; set; }
        public virtual DbSet<VwJohnCustomerActiveNew> VwJohnCustomerActiveNew { get; set; }
        public virtual DbSet<VwJohnCustomerDistinct> VwJohnCustomerDistinct { get; set; }
        public virtual DbSet<VwJohnUnionCompany> VwJohnUnionCompany { get; set; }
        public virtual DbSet<VwValidProjPhaseEvents> VwValidProjPhaseEvents { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-MNNOJHC\\SQLEXPRESS;Database=Tesco;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasKey(e => e.CompanyId)
                    .IsClustered(false);

                entity.HasIndex(e => e.Company1)
                    .HasName("IX_Company")
                    .IsClustered();

                entity.Property(e => e.CompanyId).HasColumnName("company_id");

                entity.Property(e => e.Company1)
                    .IsRequired()
                    .HasColumnName("company")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContactGroups>(entity =>
            {
                entity.HasKey(e => new { e.ContactId, e.GroupDesc })
                    .HasName("aaaaaContact_groups_PK")
                    .IsClustered(false);

                entity.ToTable("Contact_groups");

                entity.HasIndex(e => e.ContactGroupId)
                    .HasName("Contact_group_id");

                entity.HasIndex(e => e.ContactId)
                    .HasName("ContactsContact_Groups");

                entity.HasIndex(e => e.GroupDesc)
                    .HasName("GroupsContact_Groups");

                entity.Property(e => e.ContactId).HasColumnName("Contact_ID");

                entity.Property(e => e.GroupDesc)
                    .HasColumnName("Group_desc")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactGroupId)
                    .HasColumnName("Contact_group_id")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ContactNotes>(entity =>
            {
                entity.HasKey(e => e.NoteId)
                    .HasName("aaaaaContact_notes_PK")
                    .IsClustered(false);

                entity.ToTable("Contact_notes");

                entity.HasIndex(e => e.ContactId)
                    .HasName("ContactsContact_notes");

                entity.HasIndex(e => e.NoteId)
                    .HasName("noteID");

                entity.Property(e => e.NoteId).HasColumnName("noteID");

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.Notedate)
                    .HasColumnName("notedate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Notedesc)
                    .IsRequired()
                    .HasColumnName("notedesc")
                    .HasColumnType("text");

                entity.Property(e => e.UserLogin)
                    .IsRequired()
                    .HasColumnName("user_login")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Contacts>(entity =>
            {
                entity.HasKey(e => e.ContactId);

                entity.HasIndex(e => e.City)
                    .HasName("IX_Contacts_1");

                entity.HasIndex(e => e.Company)
                    .HasName("IX_Contacts");

                entity.HasIndex(e => e.ContactType)
                    .HasName("idx_contactType");

                entity.HasIndex(e => e.Department)
                    .HasName("IX_Contacts_6");

                entity.HasIndex(e => e.Fname)
                    .HasName("IX_Contacts_4");

                entity.HasIndex(e => e.Lname)
                    .HasName("IX_Contacts_5");

                entity.HasIndex(e => e.State)
                    .HasName("IX_Contacts_2");

                entity.HasIndex(e => e.Zip)
                    .HasName("IX_Contacts_3");

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.Addr1)
                    .HasColumnName("addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Addr2)
                    .HasColumnName("addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Addr1)
                    .HasColumnName("alt1_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Addr2)
                    .HasColumnName("alt1_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1City)
                    .HasColumnName("alt1_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1State)
                    .HasColumnName("alt1_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Zip)
                    .HasColumnName("alt1_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Addr1)
                    .HasColumnName("alt2_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Addr2)
                    .HasColumnName("alt2_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2City)
                    .HasColumnName("alt2_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2State)
                    .HasColumnName("alt2_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Zip)
                    .HasColumnName("alt2_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Altphone)
                    .HasColumnName("altphone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AltphoneExt)
                    .HasColumnName("altphone_ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactType)
                    .HasColumnName("contact_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("createdate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Editdate)
                    .HasColumnName("editdate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ext)
                    .HasColumnName("ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fname)
                    .HasColumnName("fname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.History)
                    .HasColumnName("history")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Homephone)
                    .HasColumnName("homephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lname)
                    .HasColumnName("lname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasColumnName("login")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mobilephone)
                    .HasColumnName("mobilephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Pager)
                    .HasColumnName("pager")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxContact).HasColumnName("trimax_contact");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasColumnName("zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DefaultPaths>(entity =>
            {
                entity.HasKey(e => e.KeyId)
                    .HasName("PK_dbo.DefaultPaths");

                entity.Property(e => e.KeyId)
                    .HasColumnName("key_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.TescoCompanyDir)
                    .HasColumnName("tesco_company_dir")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TescoCompanyDrive)
                    .HasColumnName("tesco_company_drive")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TescoProjectsDir)
                    .HasColumnName("tesco_projects_dir")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TescoReportDir)
                    .HasColumnName("tesco_report_dir")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxCompanyDir)
                    .HasColumnName("trimax_company_dir")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxCompanyDrive)
                    .HasColumnName("trimax_company_drive")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxProjectsDir)
                    .HasColumnName("trimax_projects_dir")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxReportDir)
                    .HasColumnName("trimax_report_dir")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DepartmentSps>(entity =>
            {
                entity.HasKey(e => e.DeptSpId)
                    .HasName("PK_Department_SP");

                entity.ToTable("Department_SPs");

                entity.Property(e => e.DeptSpId).HasColumnName("Dept_SP_ID");

                entity.Property(e => e.DisplayName)
                    .HasColumnName("Display_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EventSp)
                    .HasColumnName("Event_SP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkDeptartmentId).HasColumnName("fkDeptartment_ID");
            });

            modelBuilder.Entity<Departments>(entity =>
            {
                entity.HasKey(e => e.DepartmentId)
                    .HasName("aaaaaDepartments_PK")
                    .IsClustered(false);

                entity.Property(e => e.DepartmentId).HasColumnName("department_id");

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasColumnName("department_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DocumentCategories>(entity =>
            {
                entity.HasKey(e => e.CategoryId)
                    .HasName("aaaaaDocument_categories_PK")
                    .IsClustered(false);

                entity.ToTable("Document_categories");

                entity.HasIndex(e => e.CategoryId)
                    .HasName("Category_ID");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxCategory).HasColumnName("trimax_category");
            });

            modelBuilder.Entity<DocumentTypes>(entity =>
            {
                entity.HasKey(e => e.DocumentTypeId)
                    .IsClustered(false);

                entity.ToTable("document_types");

                entity.Property(e => e.DocumentTypeId).HasColumnName("document_type_id");

                entity.Property(e => e.DocumentType)
                    .HasColumnName("document_type")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Documents>(entity =>
            {
                entity.HasKey(e => e.DocumentId)
                    .HasName("aaaaaDocuments_PK")
                    .IsClustered(false);

                entity.HasIndex(e => e.Author)
                    .HasName("IX_Documents_1");

                entity.HasIndex(e => e.ContactDocnum)
                    .HasName("contact_docnum");

                entity.HasIndex(e => e.ContactId)
                    .HasName("Documentscontact_id");

                entity.HasIndex(e => e.DocumentType)
                    .HasName("Documentsdocument_type");

                entity.HasIndex(e => e.ProjectDocnum)
                    .HasName("project_docnum");

                entity.HasIndex(e => e.ProjectNumber)
                    .HasName("Documentsproject_id");

                entity.HasIndex(e => e.Subject)
                    .HasName("IX_Documents");

                entity.Property(e => e.DocumentId).HasColumnName("document_id");

                entity.Property(e => e.Author)
                    .HasColumnName("author")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ContactDocnum).HasColumnName("contact_docnum");

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DocCoAmount)
                    .HasColumnName("doc_CO_Amount")
                    .HasColumnType("numeric(9, 2)");

                entity.Property(e => e.Docfilename)
                    .HasColumnName("docfilename")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Docfilepath)
                    .HasColumnName("docfilepath")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentStatus)
                    .HasColumnName("document_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentType)
                    .HasColumnName("document_type")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('DOC')");

                entity.Property(e => e.DocumentTypeSeqno).HasColumnName("document_type_seqno");

                entity.Property(e => e.ProjectDocnum).HasColumnName("project_docnum");

                entity.Property(e => e.ProjectNumber)
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RfiId).HasColumnName("rfi_id");

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Groups>(entity =>
            {
                entity.HasKey(e => e.GroupId)
                    .HasName("aaaaaGroups_PK")
                    .IsClustered(false);

                entity.HasIndex(e => e.GroupDesc)
                    .HasName("Group_desc")
                    .IsUnique();

                entity.HasIndex(e => e.GroupId)
                    .HasName("Group_id");

                entity.Property(e => e.GroupId).HasColumnName("Group_id");

                entity.Property(e => e.GroupDesc)
                    .IsRequired()
                    .HasColumnName("Group_desc")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InfocenterPrjcontacts>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("infocenter_prjcontacts");

                entity.Property(e => e.Docfilepath)
                    .HasColumnName("docfilepath")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasColumnName("project_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MsProjectsSearch>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("msProjectsSearch");

                entity.Property(e => e.Drafter)
                    .HasColumnName("drafter")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ElectricalEngineer)
                    .HasColumnName("electrical_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmassTechnician)
                    .HasColumnName("emass_technician")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineer)
                    .HasColumnName("field_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Om)
                    .HasColumnName("om")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrjAddr1)
                    .HasColumnName("prj_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrjAddr2)
                    .HasColumnName("prj_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrjCity)
                    .HasColumnName("prj_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrjState)
                    .HasColumnName("prj_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjZip)
                    .HasColumnName("prj_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Programmer)
                    .HasColumnName("programmer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectEngineer)
                    .HasColumnName("project_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectManager)
                    .HasColumnName("project_manager")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasColumnName("project_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectOwner)
                    .HasColumnName("project_owner")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Salesman)
                    .HasColumnName("salesman")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SegEngineer)
                    .HasColumnName("seg_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SystemsProgrammer)
                    .HasColumnName("systems_programmer")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OtherInformation>(entity =>
            {
                entity.HasKey(e => e.PrimaryKeyId);

                entity.ToTable("Other_Information");

                entity.Property(e => e.PrimaryKeyId).HasColumnName("primary_key_id");

                entity.Property(e => e.ReleaseNotesLink)
                    .HasColumnName("release_notes_link")
                    .IsUnicode(false);

                entity.Property(e => e.ReportIssueLink)
                    .HasColumnName("report_issue_link")
                    .IsUnicode(false);

                entity.Property(e => e.SendEmailFromSvr).HasColumnName("send_email_from_svr");

                entity.Property(e => e.TescoCompanyDrive)
                    .HasColumnName("tesco_company_drive")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxCompanyDrive)
                    .HasColumnName("trimax_company_drive")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PlcTypes>(entity =>
            {
                entity.HasKey(e => e.PlcTypeId);

                entity.ToTable("PLC_types");

                entity.Property(e => e.PlcTypeId).HasColumnName("plc_type_id");

                entity.Property(e => e.PlcFullName)
                    .HasColumnName("plc_full_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PlcName)
                    .HasColumnName("plc_name")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("productView");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Expr2).HasColumnType("money");
            });

            modelBuilder.Entity<ProjectContactHistory>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("project_Contact_History");

                entity.Property(e => e.HistContact)
                    .IsRequired()
                    .HasColumnName("Hist_Contact")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HistContactDate)
                    .HasColumnName("Hist_Contact_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HistDetail)
                    .HasColumnName("Hist_Detail")
                    .HasColumnType("text");

                entity.Property(e => e.HistDuration)
                    .HasColumnName("Hist_Duration")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HistEntryDate)
                    .HasColumnName("Hist_Entry_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HistId)
                    .HasColumnName("Hist_ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.HistPhone)
                    .HasColumnName("Hist_Phone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.HistRegarding)
                    .IsRequired()
                    .HasColumnName("Hist_Regarding")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HistSetting)
                    .IsRequired()
                    .HasColumnName("Hist_Setting")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HistUser)
                    .IsRequired()
                    .HasColumnName("Hist_User")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PrjNumber)
                    .IsRequired()
                    .HasColumnName("prj_Number")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectContactTypes>(entity =>
            {
                entity.HasKey(e => e.ContactTypeId)
                    .HasName("aaaaaProject_contact_types_PK")
                    .IsClustered(false);

                entity.ToTable("Project_contact_types");

                entity.HasIndex(e => e.ContactType)
                    .HasName("Project_Contact_typescontact_t");

                entity.HasIndex(e => e.ContactTypeId)
                    .HasName("Contact_Typescontact_type_id")
                    .IsUnique();

                entity.Property(e => e.ContactTypeId).HasColumnName("contact_type_id");

                entity.Property(e => e.ContactType)
                    .HasColumnName("contact_type")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectContacts>(entity =>
            {
                entity.HasKey(e => new { e.ContactId, e.ProjectNumber, e.ContactTypeId })
                    .IsClustered(false);

                entity.ToTable("Project_contacts");

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.ProjectNumber)
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ContactTypeId).HasColumnName("contact_type_ID");

                entity.Property(e => e.ProjectContactId)
                    .HasColumnName("project_contact_id")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ProjectContactsOld>(entity =>
            {
                entity.HasKey(e => e.ProjectContactId)
                    .HasName("aaaaaProject_contacts_PK")
                    .IsClustered(false);

                entity.ToTable("Project_contacts_old");

                entity.HasIndex(e => e.ContactId)
                    .HasName("Project_Contactscontact_id");

                entity.HasIndex(e => e.ContactTypeId)
                    .HasName("Project_Contactscontact_type_i");

                entity.Property(e => e.ProjectContactId).HasColumnName("project_contact_id");

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.ContactTypeId).HasColumnName("contact_type_ID");

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectEventTypes>(entity =>
            {
                entity.HasKey(e => e.ProjectEventTypeId);

                entity.ToTable("Project_Event_Types");

                entity.Property(e => e.ProjectEventTypeId).HasColumnName("Project_Event_Type_ID");

                entity.Property(e => e.EventTypeAbbrev)
                    .IsRequired()
                    .HasColumnName("Event_Type_Abbrev")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.EventTypeName)
                    .HasColumnName("Event_Type_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SetDuration).HasColumnName("Set_Duration");

                entity.Property(e => e.SortOrder).HasColumnName("Sort_Order");
            });

            modelBuilder.Entity<ProjectEvents>(entity =>
            {
                entity.HasKey(e => e.ProjectEventId);

                entity.ToTable("Project_Events");

                entity.Property(e => e.ProjectEventId).HasColumnName("Project_Event_ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("date");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("Created_By")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.EventDate)
                    .HasColumnName("Event_Date")
                    .HasColumnType("date");

                entity.Property(e => e.FkProjectEventTypeId).HasColumnName("fkProject_Event_Type_ID");

                entity.Property(e => e.FkProjectPhaseId).HasColumnName("fkProject_Phase_ID");

                entity.HasOne(d => d.FkProjectEventType)
                    .WithMany(p => p.ProjectEvents)
                    .HasForeignKey(d => d.FkProjectEventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Project_Events_Project_Event_Types");

                entity.HasOne(d => d.FkProjectPhase)
                    .WithMany(p => p.ProjectEvents)
                    .HasForeignKey(d => d.FkProjectPhaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Project_Events_Project_Phases");
            });

            modelBuilder.Entity<ProjectNotes>(entity =>
            {
                entity.HasKey(e => e.NoteId)
                    .HasName("aaaaaProject_notes_PK")
                    .IsClustered(false);

                entity.ToTable("Project_notes");

                entity.HasIndex(e => e.NoteId)
                    .HasName("noteID");

                entity.HasIndex(e => e.ProjectNumber)
                    .HasName("contact_id");

                entity.Property(e => e.NoteId).HasColumnName("noteID");

                entity.Property(e => e.Notedate)
                    .HasColumnName("notedate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Notedesc)
                    .IsRequired()
                    .HasColumnName("notedesc")
                    .HasColumnType("text");

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserLogin)
                    .IsRequired()
                    .HasColumnName("user_login")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.ProjectNumberNavigation)
                    .WithMany(p => p.ProjectNotes)
                    .HasForeignKey(d => d.ProjectNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Project_notes_Projects");
            });

            modelBuilder.Entity<ProjectOm>(entity =>
            {
                entity.HasKey(e => e.Omid);

                entity.ToTable("Project_OM");

                entity.Property(e => e.Omid).HasColumnName("OMID");

                entity.Property(e => e.Omcomments)
                    .HasColumnName("OMComments")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OmhardcopyCnt).HasColumnName("OMHardcopyCnt");

                entity.Property(e => e.OmpdfCnt).HasColumnName("OMPdfCnt");

                entity.Property(e => e.OmprojectNumber)
                    .IsRequired()
                    .HasColumnName("OMProject_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OmreleaseDate)
                    .HasColumnName("OMReleaseDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Omshipto).HasColumnName("OMShipto");

                entity.Property(e => e.Omstatus)
                    .HasColumnName("OMStatus")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OmtocDate)
                    .HasColumnName("OMTocDate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<ProjectParts>(entity =>
            {
                entity.HasKey(e => e.PrjPartsId);

                entity.ToTable("project_parts");

                entity.Property(e => e.PrjPartsId).HasColumnName("prj_parts_ID");

                entity.Property(e => e.PrjAmtOwed)
                    .HasColumnName("prj_amt_owed")
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PrjApprRcvdDate)
                    .HasColumnName("prj_appr_rcvd_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjBilledAmt)
                    .HasColumnName("prj_billed_amt")
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PrjComments)
                    .HasColumnName("prj_comments")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrjContractAmt)
                    .HasColumnName("prj_contract_amt")
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PrjDateOrd)
                    .HasColumnName("prj_date_ord")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjDocsNeeded)
                    .HasColumnName("prj_docs_needed")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjDrawDate)
                    .HasColumnName("prj_draw_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjEndNeedreleasedDate)
                    .HasColumnName("prj_end_needreleased_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjEngRel)
                    .HasColumnName("prj_eng_rel")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjEngineerEe)
                    .HasColumnName("prj_engineer_ee")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PrjFieldWork)
                    .HasColumnName("prj_field_work")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjHistoryDate)
                    .HasColumnName("prj_history_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjLocation)
                    .HasColumnName("prj_location")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrjMaterialDate)
                    .HasColumnName("prj_material_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjOfa)
                    .HasColumnName("prj_ofa")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjPcntMatl)
                    .HasColumnName("prj_pcnt_matl")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.PrjPlcCnt)
                    .HasColumnName("prj_plc_cnt")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PrjPriContactId).HasColumnName("prj_pri_contact_ID");

                entity.Property(e => e.PrjRlsShopDate)
                    .HasColumnName("prj_rls_shop_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjRqstShipDate)
                    .HasColumnName("prj_rqst_ship_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjScada)
                    .HasColumnName("prj_scada")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PrjSchdShipDate)
                    .HasColumnName("prj_schd_ship_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjShipStat)
                    .HasColumnName("prj_ship_stat")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjStartNeedreleasedDate)
                    .HasColumnName("prj_start_needreleased_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjStatus)
                    .HasColumnName("prj_status")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjSubcc)
                    .HasColumnName("prj_subcc")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjTypeProducts)
                    .HasColumnName("prj_type_products")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrjVerifiedReqshipdate).HasColumnName("prj_verified_reqshipdate");

                entity.Property(e => e.PrjWarrantyDate)
                    .HasColumnName("prj_warranty_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectStatusId).HasColumnName("project_status_ID");

                entity.Property(e => e.ProjectTypeId).HasColumnName("project_type_ID");
            });

            modelBuilder.Entity<ProjectPhases>(entity =>
            {
                entity.HasKey(e => e.ProjectPhaseId);

                entity.ToTable("Project_Phases");

                entity.Property(e => e.ProjectPhaseId).HasColumnName("Project_Phase_ID");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("date");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("Created_By")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.FkprojectNumber)
                    .IsRequired()
                    .HasColumnName("fkproject_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PhaseName)
                    .IsRequired()
                    .HasColumnName("Phase_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhaseNumber).HasColumnName("Phase_Number");
            });

            modelBuilder.Entity<ProjectSecurity>(entity =>
            {
                entity.HasKey(e => e.Projectid);

                entity.ToTable("Project_Security");

                entity.Property(e => e.Projectid).HasColumnName("projectid");

                entity.Property(e => e.Changedate)
                    .HasColumnName("CHANGEDATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Currentuser)
                    .HasColumnName("CURRENTUSER")
                    .HasMaxLength(50);

                entity.Property(e => e.Newcontractamount)
                    .HasColumnName("NEWCONTRACTAMOUNT")
                    .HasColumnType("money");

                entity.Property(e => e.Newstatus)
                    .HasColumnName("NEWSTATUS")
                    .HasMaxLength(50);

                entity.Property(e => e.Prevcontractamount)
                    .HasColumnName("PREVCONTRACTAMOUNT")
                    .HasColumnType("money");

                entity.Property(e => e.Prevstatus)
                    .HasColumnName("PREVSTATUS")
                    .HasMaxLength(50);

                entity.Property(e => e.Projectnumber)
                    .IsRequired()
                    .HasColumnName("PROJECTNUMBER")
                    .HasMaxLength(50);

                entity.Property(e => e.Recnum)
                    .HasColumnName("RECNUM")
                    .HasMaxLength(10)
                    .IsFixedLength();
            });

            modelBuilder.Entity<ProjectStatus>(entity =>
            {
                entity.HasKey(e => e.ProjectStatusId)
                    .HasName("aaaaaProject_status_PK")
                    .IsClustered(false);

                entity.ToTable("Project_status");

                entity.HasIndex(e => e.ProjectStatus1)
                    .HasName("Project_Statusproject_status");

                entity.HasIndex(e => e.ProjectStatusId)
                    .HasName("Project_Status_ID");

                entity.Property(e => e.ProjectStatusId).HasColumnName("Project_Status_ID");

                entity.Property(e => e.ProjectCatCodes)
                    .HasColumnName("Project_Cat_Codes")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectStatus1)
                    .IsRequired()
                    .HasColumnName("Project_Status")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectTypes>(entity =>
            {
                entity.HasKey(e => e.ProjectTypeId)
                    .HasName("aaaaaProject_types_PK")
                    .IsClustered(false);

                entity.ToTable("Project_types");

                entity.HasIndex(e => e.ProjectType)
                    .HasName("Project_typesproject_type");

                entity.HasIndex(e => e.ProjectTypeId)
                    .HasName("Project_Type_ID");

                entity.Property(e => e.ProjectTypeId).HasColumnName("Project_Type_ID");

                entity.Property(e => e.ProjectType)
                    .IsRequired()
                    .HasColumnName("Project_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Projects>(entity =>
            {
                entity.HasKey(e => e.ProjectNumber)
                    .HasName("aaaaaProjects_PK");

                entity.HasIndex(e => e.ProjectName)
                    .HasName("IX_Projects");

                entity.Property(e => e.ProjectNumber)
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjAddr1)
                    .HasColumnName("prj_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrjAddr2)
                    .HasColumnName("prj_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrjCity)
                    .HasColumnName("prj_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrjPhone)
                    .HasColumnName("prj_phone")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrjState)
                    .HasColumnName("prj_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjZip)
                    .HasColumnName("prj_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasColumnName("project_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectOwner)
                    .HasColumnName("project_owner")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectPo)
                    .HasColumnName("project_po")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectState)
                    .HasColumnName("project_state")
                    .HasMaxLength(8)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Rfi>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rfi");

                entity.Property(e => e.ActionComments)
                    .HasColumnName("action_comments")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AnsweredStamp)
                    .HasColumnName("answered_stamp")
                    .HasColumnType("datetime");

                entity.Property(e => e.Bic)
                    .HasColumnName("bic")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.PendingStamp)
                    .HasColumnName("pending_stamp")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Response)
                    .HasColumnName("response")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RfiId)
                    .HasColumnName("rfi_id")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ScadaTypes>(entity =>
            {
                entity.HasKey(e => e.ScadaTypeId);

                entity.ToTable("SCADA_types");

                entity.Property(e => e.ScadaTypeId).HasColumnName("scada_type_id");

                entity.Property(e => e.ScadaFullName)
                    .HasColumnName("scada_full_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ScadaName)
                    .HasColumnName("scada_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblMasterBom1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblMasterBOM1");

                entity.Property(e => e.Assembly).HasMaxLength(10);

                entity.Property(e => e.DateCh)
                    .HasColumnName("Date_Ch")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCr)
                    .HasColumnName("Date_Cr")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.DevType)
                    .HasColumnName("Dev_Type")
                    .HasMaxLength(15);

                entity.Property(e => e.FromProject).HasMaxLength(10);

                entity.Property(e => e.ItemId)
                    .HasColumnName("ItemID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.PartNumber).HasMaxLength(50);

                entity.Property(e => e.PriceB)
                    .HasColumnName("Price_B")
                    .HasColumnType("money");

                entity.Property(e => e.PriceS)
                    .HasColumnName("Price_S")
                    .HasColumnType("money");

                entity.Property(e => e.SymbRef)
                    .HasColumnName("Symb_ref")
                    .HasMaxLength(10);

                entity.Property(e => e.Techdata).HasMaxLength(60);
            });

            modelBuilder.Entity<TblProjectTeam>(entity =>
            {
                entity.HasKey(e => e.ProjectNumber)
                    .HasName("aaaaatblProject_team_PK")
                    .IsClustered(false);

                entity.ToTable("tblProject_team");

                entity.HasIndex(e => e.ProjectNumber)
                    .HasName("ProjectstblProject_team")
                    .IsUnique();

                entity.Property(e => e.ProjectNumber)
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Drafter)
                    .HasColumnName("drafter")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DrafterIni)
                    .HasColumnName("drafter_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.DrafterNotify)
                    .HasColumnName("drafter_notify")
                    .IsUnicode(false);

                entity.Property(e => e.ElectricalEngineer)
                    .HasColumnName("electrical_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ElectricalEngineerIni)
                    .HasColumnName("electrical_engineer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ElectricalEngineerNotify)
                    .HasColumnName("electrical_engineer_notify")
                    .IsUnicode(false);

                entity.Property(e => e.EmassTechnician)
                    .HasColumnName("emass_technician")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmassTechnicianIni)
                    .HasColumnName("emass_technician_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.EmassTechnicianNotify)
                    .HasColumnName("emass_technician_notify")
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineer)
                    .HasColumnName("field_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineer2)
                    .HasColumnName("field_engineer2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineer2Ini)
                    .HasColumnName("field_engineer2_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineer2Notify)
                    .HasColumnName("field_engineer2_notify")
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineerIni)
                    .HasColumnName("field_engineer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineerNotify)
                    .HasColumnName("field_engineer_notify")
                    .IsUnicode(false);

                entity.Property(e => e.Om)
                    .HasColumnName("om")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OmIni)
                    .HasColumnName("om_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.OmNotify)
                    .HasColumnName("om_notify")
                    .IsUnicode(false);

                entity.Property(e => e.Programmer)
                    .HasColumnName("programmer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProgrammerIni)
                    .HasColumnName("programmer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ProgrammerNotify)
                    .HasColumnName("programmer_notify")
                    .IsUnicode(false);

                entity.Property(e => e.ProjectEngineer)
                    .HasColumnName("project_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectEngineerIni)
                    .HasColumnName("project_engineer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectEngineerNotify)
                    .HasColumnName("project_engineer_notify")
                    .IsUnicode(false);

                entity.Property(e => e.ProjectManager)
                    .HasColumnName("project_manager")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectManagerIni)
                    .HasColumnName("project_manager_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectManagerNotify)
                    .HasColumnName("project_manager_notify")
                    .IsUnicode(false);

                entity.Property(e => e.Salesman)
                    .HasColumnName("salesman")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SalesmanIni)
                    .HasColumnName("salesman_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SalesmanNotify)
                    .HasColumnName("salesman_notify")
                    .IsUnicode(false);

                entity.Property(e => e.SegEngineer)
                    .HasColumnName("seg_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SegEngineerIni)
                    .HasColumnName("seg_engineer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SegEngineerNotify)
                    .HasColumnName("seg_engineer_notify")
                    .IsUnicode(false);

                entity.Property(e => e.SpecialProjEngineer)
                    .HasColumnName("special_proj_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpecialProjEngineerIni)
                    .HasColumnName("special_proj_engineer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SpecialProjEngineerNotify)
                    .HasColumnName("special_proj_engineer_notify")
                    .IsUnicode(false);

                entity.Property(e => e.Subscriber1)
                    .HasColumnName("subscriber1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Subscriber1Ini)
                    .HasColumnName("subscriber1_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Subscriber1Notify)
                    .HasColumnName("subscriber1_notify")
                    .IsUnicode(false);

                entity.Property(e => e.SystemsProgrammer)
                    .HasColumnName("systems_programmer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SystemsProgrammerIni)
                    .HasColumnName("systems_programmer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SystemsProgrammerNotify)
                    .HasColumnName("systems_programmer_notify")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblVendors>(entity =>
            {
                entity.HasKey(e => e.VendorId)
                    .HasName("aaaaatblVendors_PK")
                    .IsClustered(false);

                entity.ToTable("tblVendors");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.Addr1)
                    .HasColumnName("addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Addr2)
                    .HasColumnName("addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Addr3)
                    .HasColumnName("addr3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AltPhone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Altaddr1)
                    .HasColumnName("altaddr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Altaddr2)
                    .HasColumnName("altaddr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Altcity)
                    .HasColumnName("altcity")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AltphoneExt)
                    .HasColumnName("altphone_ext")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Altstate)
                    .HasColumnName("altstate")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Altzip)
                    .HasColumnName("altzip")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ext)
                    .HasColumnName("ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fname)
                    .HasColumnName("fname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.HomePhone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lname)
                    .HasColumnName("lname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Pager)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Templates>(entity =>
            {
                entity.HasKey(e => e.Templateid)
                    .HasName("aaaaaTemplates_PK")
                    .IsClustered(false);

                entity.HasIndex(e => e.CategoryId)
                    .HasName("_F655C2E5_54AC_11D3_B9FA_90884");

                entity.HasIndex(e => e.Templateid)
                    .HasName("file_id");

                entity.Property(e => e.Templateid).HasColumnName("templateid");

                entity.Property(e => e.CategoryId).HasColumnName("categoryID");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Doctype)
                    .HasColumnName("doctype")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FilePathName)
                    .HasColumnName("filePathName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SubjectText)
                    .HasColumnName("subjectText")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxTemplate).HasColumnName("trimax_template");

                entity.Property(e => e.UseSubjectText)
                    .HasColumnName("useSubjectText")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserLogin)
                    .HasName("aaaaaUsers_PK")
                    .IsClustered(false);

                entity.HasIndex(e => e.DepartmentId)
                    .HasName("department_id");

                entity.HasIndex(e => new { e.UserLogin, e.Password })
                    .HasName("IX_Users");

                entity.Property(e => e.UserLogin)
                    .HasColumnName("user_login")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.BomcpuName)
                    .HasColumnName("BOMcpuName")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Computername)
                    .HasColumnName("computername")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("department_id")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Disabled).HasDefaultValueSql("((0))");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Extension)
                    .HasColumnName("extension")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Faxnum)
                    .HasColumnName("faxnum")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('password')");

                entity.Property(e => e.Seclevel)
                    .HasColumnName("seclevel")
                    .HasDefaultValueSql("((5))");

                entity.Property(e => e.Signaturefile)
                    .HasColumnName("signaturefile")
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TrimaxAccess).HasColumnName("trimax_access");

                entity.Property(e => e.UpdateNotify).HasColumnName("update_notify");

                entity.Property(e => e.UserInitials)
                    .HasColumnName("user_initials")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasColumnName("user_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserNotification)
                    .HasColumnName("user_notification")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VendorGroups>(entity =>
            {
                entity.HasKey(e => e.ContactGroupId)
                    .HasName("aaaaaVendor_groups_PK")
                    .IsClustered(false);

                entity.ToTable("Vendor_groups");

                entity.HasIndex(e => e.ContactGroupId)
                    .HasName("Contact_group_id");

                entity.HasIndex(e => e.ContactId)
                    .HasName("ContactGroupID");

                entity.HasIndex(e => e.GroupDesc)
                    .HasName("GroupID");

                entity.Property(e => e.ContactGroupId).HasColumnName("Contact_group_id");

                entity.Property(e => e.ContactId).HasColumnName("Contact_ID");

                entity.Property(e => e.GroupDesc)
                    .IsRequired()
                    .HasColumnName("Group_desc")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VendorNotes>(entity =>
            {
                entity.HasKey(e => e.NoteId)
                    .HasName("aaaaaVendor_notes_PK")
                    .IsClustered(false);

                entity.ToTable("Vendor_notes");

                entity.HasIndex(e => e.ContactId)
                    .HasName("contact_id");

                entity.HasIndex(e => e.NoteId)
                    .HasName("noteID");

                entity.Property(e => e.NoteId).HasColumnName("noteID");

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.Notedate)
                    .HasColumnName("notedate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Notedesc)
                    .HasColumnName("notedesc")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserLogin)
                    .HasColumnName("user_login")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<View1>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View1");

                entity.Property(e => e.Addr1)
                    .HasColumnName("addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Addr2)
                    .HasColumnName("addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Addr1)
                    .HasColumnName("alt1_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Addr2)
                    .HasColumnName("alt1_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1City)
                    .HasColumnName("alt1_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1State)
                    .HasColumnName("alt1_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Zip)
                    .HasColumnName("alt1_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Addr1)
                    .HasColumnName("alt2_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Addr2)
                    .HasColumnName("alt2_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2City)
                    .HasColumnName("alt2_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2State)
                    .HasColumnName("alt2_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Zip)
                    .HasColumnName("alt2_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Altphone)
                    .HasColumnName("altphone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AltphoneExt)
                    .HasColumnName("altphone_ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId).HasColumnName("contact_id");

                entity.Property(e => e.ContactType)
                    .HasColumnName("contact_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("createdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Editdate)
                    .HasColumnName("editdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ext)
                    .HasColumnName("ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fname)
                    .HasColumnName("fname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.History)
                    .HasColumnName("history")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Homephone)
                    .HasColumnName("homephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lname)
                    .HasColumnName("lname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasColumnName("login")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mobilephone)
                    .HasColumnName("mobilephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Pager)
                    .HasColumnName("pager")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxContact).HasColumnName("trimax_contact");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasColumnName("zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<View2>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View2");

                entity.Property(e => e.Addr1)
                    .HasColumnName("addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Addr2)
                    .HasColumnName("addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Addr1)
                    .HasColumnName("alt1_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Addr2)
                    .HasColumnName("alt1_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1City)
                    .HasColumnName("alt1_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1State)
                    .HasColumnName("alt1_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Zip)
                    .HasColumnName("alt1_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Addr1)
                    .HasColumnName("alt2_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Addr2)
                    .HasColumnName("alt2_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2City)
                    .HasColumnName("alt2_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2State)
                    .HasColumnName("alt2_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Zip)
                    .HasColumnName("alt2_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Altphone)
                    .HasColumnName("altphone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AltphoneExt)
                    .HasColumnName("altphone_ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasColumnName("contact_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ContactType)
                    .HasColumnName("contact_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("createdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Editdate)
                    .HasColumnName("editdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ext)
                    .HasColumnName("ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fname)
                    .HasColumnName("fname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.History)
                    .HasColumnName("history")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Homephone)
                    .HasColumnName("homephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lname)
                    .HasColumnName("lname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasColumnName("login")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mobilephone)
                    .HasColumnName("mobilephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Pager)
                    .HasColumnName("pager")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxContact).HasColumnName("trimax_contact");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasColumnName("zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViewContactGroups>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("viewContactGroups");

                entity.Property(e => e.Addr1)
                    .HasColumnName("addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Addr2)
                    .HasColumnName("addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Addr1)
                    .HasColumnName("alt1_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Addr2)
                    .HasColumnName("alt1_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1City)
                    .HasColumnName("alt1_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1State)
                    .HasColumnName("alt1_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Alt1Zip)
                    .HasColumnName("alt1_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Addr1)
                    .HasColumnName("alt2_addr1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Addr2)
                    .HasColumnName("alt2_addr2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2City)
                    .HasColumnName("alt2_city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2State)
                    .HasColumnName("alt2_state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Alt2Zip)
                    .HasColumnName("alt2_zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Altphone)
                    .HasColumnName("altphone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AltphoneExt)
                    .HasColumnName("altphone_ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasColumnName("contact_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ContactType)
                    .HasColumnName("contact_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("createdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Editdate)
                    .HasColumnName("editdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ext)
                    .HasColumnName("ext")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fname)
                    .HasColumnName("fname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.History)
                    .HasColumnName("history")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Homephone)
                    .HasColumnName("homephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lname)
                    .HasColumnName("lname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasColumnName("login")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mobilephone)
                    .HasColumnName("mobilephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Pager)
                    .HasColumnName("pager")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TrimaxContact).HasColumnName("trimax_contact");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasColumnName("zip")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwActiveNonPedProjectEvents>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwActiveNonPedProjectEvents");

                entity.Property(e => e.EventDate)
                    .HasColumnName("Event_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventStatus)
                    .HasColumnName("Event_Status")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.EventSum)
                    .HasColumnName("Event_Sum")
                    .HasMaxLength(125)
                    .IsUnicode(false);

                entity.Property(e => e.EventTypeAbbrev)
                    .HasColumnName("Event_Type_Abbrev")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrjPartsId).HasColumnName("prj_parts_ID");

                entity.Property(e => e.ProjectCatCodes)
                    .HasColumnName("Project_Cat_Codes")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectEventId).HasColumnName("Project_Event_ID");

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwActiveNonPedProjects>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwActiveNonPedProjects");

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasColumnName("project_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectOwner)
                    .HasColumnName("project_owner")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwActiveNonPedProjectsParts>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwActiveNonPedProjectsParts");

                entity.Property(e => e.PrjApprRcvdDate)
                    .HasColumnName("prj_appr_rcvd_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjComments)
                    .HasColumnName("prj_comments")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrjDateOrd)
                    .HasColumnName("prj_date_ord")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjDrawDate)
                    .HasColumnName("prj_draw_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjEndNeedreleasedDate)
                    .HasColumnName("prj_end_needreleased_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjEngRel)
                    .HasColumnName("prj_eng_rel")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjHistoryDate)
                    .HasColumnName("prj_history_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjLocation)
                    .HasColumnName("prj_location")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrjOfa)
                    .HasColumnName("prj_ofa")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjPartsId).HasColumnName("prj_parts_ID");

                entity.Property(e => e.PrjRlsShopDate)
                    .HasColumnName("prj_rls_shop_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjRqstShipDate)
                    .HasColumnName("prj_rqst_ship_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjSchdShipDate)
                    .HasColumnName("prj_schd_ship_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjShipStat)
                    .HasColumnName("prj_ship_stat")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrjStartNeedreleasedDate)
                    .HasColumnName("prj_start_needreleased_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrjStatus)
                    .HasColumnName("prj_status")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectCatCodes)
                    .HasColumnName("Project_Cat_Codes")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasColumnName("project_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectOwner)
                    .HasColumnName("project_owner")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectStatus)
                    .IsRequired()
                    .HasColumnName("Project_Status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectType)
                    .IsRequired()
                    .HasColumnName("Project_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwActiveNonPedProjectsWithTeam>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwActiveNonPedProjectsWithTeam");

                entity.Property(e => e.Drafter)
                    .HasColumnName("drafter")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DrafterIni)
                    .HasColumnName("drafter_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ElectricalEngineer)
                    .HasColumnName("electrical_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ElectricalEngineerIni)
                    .HasColumnName("electrical_engineer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineer)
                    .HasColumnName("field_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineer2)
                    .HasColumnName("field_engineer2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineer2Ini)
                    .HasColumnName("field_engineer2_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.FieldEngineerIni)
                    .HasColumnName("field_engineer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Om)
                    .HasColumnName("om")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OmIni)
                    .HasColumnName("om_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Programmer)
                    .HasColumnName("programmer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProgrammerIni)
                    .HasColumnName("programmer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectEngineer)
                    .HasColumnName("project_engineer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectEngineerIni)
                    .HasColumnName("project_engineer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectManager)
                    .HasColumnName("project_manager")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectManagerIni)
                    .HasColumnName("project_manager_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasColumnName("project_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectOwner)
                    .HasColumnName("project_owner")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Salesman)
                    .HasColumnName("salesman")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SalesmanIni)
                    .HasColumnName("salesman_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SystemsProgrammer)
                    .HasColumnName("systems_programmer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SystemsProgrammerIni)
                    .HasColumnName("systems_programmer_ini")
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwJohnCompanyHistory>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwJohnCompanyHistory");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaxPrjDateOrd)
                    .HasColumnName("max_prj_date_ord")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProjectCatCodes)
                    .HasColumnName("Project_Cat_Codes")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectNumber)
                    .IsRequired()
                    .HasColumnName("project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwJohnCompanyHistoryLatest>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwJohnCompanyHistoryLatest");

                entity.Property(e => e.CatCode)
                    .HasColumnName("cat_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaxPrjDate)
                    .HasColumnName("max_prj_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxPrjNum)
                    .HasColumnName("max_prj_num")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwJohnCompanyTotalProjects>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwJohnCompanyTotalProjects");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwJohnCurrentJobs>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwJohnCurrentJobs");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaxPrjDateOrd)
                    .HasColumnName("max_prj_date_ord")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxProjectNumber)
                    .HasColumnName("max_project_number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MinCatCode)
                    .HasColumnName("min_cat_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwJohnCustomerActiveLastHistory>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwJohnCustomerActiveLastHistory");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaxPrjDate)
                    .HasColumnName("max_prj_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MinCat)
                    .HasColumnName("min_cat")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwJohnCustomerActiveNew>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwJohnCustomerActiveNew");

                entity.Property(e => e.CatCode)
                    .HasColumnName("cat_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaxPrjDate)
                    .HasColumnName("max_prj_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxPrjNum)
                    .HasColumnName("max_prj_num")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwJohnCustomerDistinct>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwJohnCustomerDistinct");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaxPrjDate)
                    .HasColumnName("max_prj_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxPrjNum)
                    .HasColumnName("max_prj_num")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MinCat)
                    .HasColumnName("min_cat")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwJohnUnionCompany>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwJohnUnionCompany");

                entity.Property(e => e.CatCode)
                    .HasColumnName("cat_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaxPrjDate)
                    .HasColumnName("max_prj_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxPrjNum)
                    .HasColumnName("max_prj_num")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwValidProjPhaseEvents>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwValidProjPhaseEvents");

                entity.Property(e => e.FkProjectEventTypeId).HasColumnName("fkProject_Event_Type_ID");

                entity.Property(e => e.FkProjectPhaseId).HasColumnName("fkProject_Phase_ID");

                entity.Property(e => e.ProjectEventId).HasColumnName("Project_Event_ID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
