﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using TescoControls.Tesco.Data.Context;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Data.Models;
using TescoControls.Utilities.CommonClasses;
using TescoControls.Utilities.Exceptions;

namespace TescoControls.Tesco.Data.Repositories
{
    public class ContactsRepository : BaseRepository
    {
        public  ContactsRepository(
            TescoContext tescoContext, 
            ILogger<ContactsRepository> logger) 
            : base(tescoContext, logger)
        {
        }

        public async Task<Contacts> GetContactAsync(int id)
        {
            var company = await _dbContext.Contacts
                .FirstOrDefaultAsync(c => c.ContactId == id);

            if (company == null)
                throw new NotFoundException();

            return company;
        }

        public async Task<PaginatedList<ContactGetDto>> GetAsync(
            string searchPart,
            int offset,
            int pageSize,
            string sortField,
            bool sortOrder)
        {
            var query = _dbContext.Contacts
                .Where(c => c.Title.Contains(searchPart))
                .Select(c => new ContactGetDto
                {
                    Id = c.ContactId,
                    Company = c.Company,
                    Address1 = c.Addr1,
                    Address2 = c.Addr2,
                    City = c.City,
                    State = c.State,
                    Zip = c.Zip,
                    Phone = c.Phone,
                    Fax = c.Fax,
                    HomePhone = c.Homephone,
                    MobilePhone = c.Mobilephone,
                    Pager = c.Pager,
                    AlternatePhone = c.Altphone,
                    FirstName = c.Fname,
                    LastName = c.Lname,
                    Extension = c.Ext,
                    AlternatePhoneExtension = c.AltphoneExt,
                    Department = c.Department,
                    CreateDate = c.Createdate,
                    EditDate = c.Editdate,
                    Email = c.Email,
                    Url = c.Url,
                    ContactType = c.ContactType
                })
                .AsNoTracking();

            var count = await query.CountAsync();

            if (count == 0)
                throw new NotFoundException(); //todo: add additional data to this constructor

            switch (sortField)
            {
                case "name":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Company)
                            : query.OrderBy(q => q.Company);
                        break;
                    }
                case "firstname":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.FirstName)
                            : query.OrderBy(q => q.FirstName);
                        break;
                    }
                case "lastname":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.LastName)
                            : query.OrderBy(q => q.LastName);
                        break;
                    }
                case "city":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.City)
                            : query.OrderBy(q => q.City);
                        break;
                    }
                case "state":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.State)
                            : query.OrderBy(q => q.State);
                        break;
                    }
                case "zip":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Zip)
                            : query.OrderBy(q => q.Zip);
                        break;
                    }
                case "extension":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Extension)
                            : query.OrderBy(q => q.Extension);
                        break;
                    }
                case "createdate":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.CreateDate)
                            : query.OrderBy(q => q.CreateDate);
                        break;
                    }
                case "editdate":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.EditDate)
                            : query.OrderBy(q => q.EditDate);
                        break;
                    }
                default:
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Id)
                            : query.OrderBy(q => q.Id);
                        break;
                    }
            }

            var records = await query
                .Skip(offset)
                .Take(pageSize)
                .ToListAsync();

            var result = new PaginatedList<ContactGetDto>
            {
                Records = records,
                Count = count
            };

            return result;
        }

        public async Task<ContactDto> GetAsync(int id)
        {
            var company = await _dbContext.Contacts
                .Where(c => c.ContactId == id)
                .Select(c => new ContactDto
                {
                    Id = c.ContactId,
                    Company = c.Company,
                    Address1 = c.Addr1,
                    Address2 = c.Addr2,
                    City = c.City,
                    State = c.State,
                    Zip = c.Zip,
                    Phone = c.Phone,
                    Fax = c.Fax,
                    HomePhone = c.Homephone,
                    MobilePhone = c.Mobilephone,
                    Pager = c.Pager,
                    AlternatePhone = c.Altphone,
                    FirstName = c.Fname,
                    LastName = c.Lname,
                    Extension = c.Ext,
                    AlternatePhoneExtension = c.AltphoneExt,
                    Department = c.Department,
                    CreateDate = c.Createdate,
                    EditDate = c.Editdate,
                    Email = c.Email,
                    Url = c.Url,
                    ContactType = c.ContactType,
                    AlternateFirstAddress1 = c.Alt1Addr1,
                    AlternateFirstAddress2 = c.Alt1Addr2,
                    AlternateFirstCity = c.Alt1City,
                    AlternateFirstState = c.Alt1State,
                    AlternateFirstZip = c.Alt1Zip,
                    AlternateSecondAddress1 = c.Alt2Addr2,
                    AlternateSecondAddress2 = c.Alt2Addr2,
                    AlternateSecondCity = c.Alt2City,
                    AlternateSecondState = c.Alt2State,
                    AlternateSecondZip = c.Alt2Zip,
                    History = c.History,
                    Login = c.Login,
                    TrimaxContact = c.TrimaxContact
                })
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if (company == null)
                throw new NotFoundException();

            return company;
        }

        public async Task<bool> CreateAsync(Contacts contact)
        {
            await _dbContext.Contacts.AddAsync(contact);
            _dbContext.SaveChanges();

            return true;
        }

        public bool Update(Contacts contact)
        {
            _dbContext.Contacts.Update(contact);
            _dbContext.SaveChanges();

            return true;
        }

        public bool Delete(Contacts contact)
        {
            _dbContext.Contacts.Remove(contact);
            _dbContext.SaveChanges();

            return true;
        }
    }
}
