﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using TescoControls.Tesco.Data.Context;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Data.Models;
using TescoControls.Utilities.CommonClasses;
using TescoControls.Utilities.Exceptions;

namespace TescoControls.Tesco.Data.Repositories
{
    public class DocumentsRepository : BaseRepository
    {
        public DocumentsRepository(
            TescoContext tescoContext, 
            ILogger<DocumentsRepository> logger) 
            : base(tescoContext, logger)
        {
        }

        public async Task<Documents> GetDocumentAsync(int id)
        {
            var document = await _dbContext.Documents
                .FirstOrDefaultAsync(d => d.DocumentId == id);

            if (document == null)
                throw new NotFoundException();

            return document;
        }

        public async Task<PaginatedList<DocumentDto>> GetAsync(
            string searchPart,
            int offset,
            int pageSize,
            string sortField,
            bool sortOrder)
        {
            var query = _dbContext.Documents
                .Where(d => d.ProjectNumber.Contains(searchPart))
                .Select(d => new DocumentDto
                {
                    Id = d.DocumentId,
                    ProjectNumber = d.ProjectNumber,
                    DocumentType = d.DocumentType,
                    Subject = d.Subject,
                    DateCreated = d.CreateDate,
                    Author = d.Author,
                    DocumentStatus = d.DocumentStatus,
                    FileName = d.Docfilename,
                    FilePath = d.Docfilepath
                })
                .AsNoTracking();

            var count = await query.CountAsync();

            if (count == 0)
                throw new NotFoundException(); //todo: add additional data to this constructor

            switch (sortField)
            {
                case "projectnumber":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.ProjectNumber)
                            : query.OrderBy(q => q.ProjectNumber);
                        break;
                    }
                case "createdate":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.DateCreated)
                            : query.OrderBy(q => q.DateCreated);
                        break;
                    }
                case "author":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Author)
                            : query.OrderBy(q => q.Author);
                        break;
                    }
                case "docfilename":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.FileName)
                            : query.OrderBy(q => q.FileName);
                        break;
                    }
                default:
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Id)
                            : query.OrderBy(q => q.Id);
                        break;
                    }
            }

            var records = await query
                .Skip(offset)
                .Take(pageSize)
                .ToListAsync();

            var result = new PaginatedList<DocumentDto>
            {
                Records = records,
                Count = count
            };

            return result;
        }

        public PaginatedList<DocumentDto> Get(
            string searchPart,
            int offset,
            int pageSize,
            string sortField,
            bool sortOrder)
        {
            var query = _dbContext.Documents
                .Where(d => d.ProjectNumber.Contains(searchPart))
                .Select(d => new DocumentDto
                {
                    Id = d.DocumentId,
                    ProjectNumber = d.ProjectNumber,
                    DocumentType = d.DocumentType,
                    Subject = d.Subject,
                    DateCreated = d.CreateDate,
                    Author = d.Author,
                    DocumentStatus = d.DocumentStatus,
                    FileName = d.Docfilename,
                    FilePath = d.Docfilepath
                })
                .AsNoTracking();

            var count = query.Count();

            if (count == 0)
                throw new NotFoundException(); //todo: add additional data to this constructor

            switch (sortField)
            {
                case "projectnumber":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.ProjectNumber)
                            : query.OrderBy(q => q.ProjectNumber);
                        break;
                    }
                case "createdate":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.DateCreated)
                            : query.OrderBy(q => q.DateCreated);
                        break;
                    }
                case "author":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Author)
                            : query.OrderBy(q => q.Author);
                        break;
                    }
                case "docfilename":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.FileName)
                            : query.OrderBy(q => q.FileName);
                        break;
                    }
                default:
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Id)
                            : query.OrderBy(q => q.Id);
                        break;
                    }
            }

            var records = query
                .Skip(offset)
                .Take(pageSize)
                .ToList();

            var result = new PaginatedList<DocumentDto>
            {
                Records = records,
                Count = count
            };

            return result;
        }

        public async Task<DocumentDto> GetAsync(int id)
        {
            var document = await _dbContext.Documents
                .Where(d => d.DocumentId == id)
                .Select(d => new DocumentDto
                {
                    Id = d.DocumentId,
                    ProjectNumber = d.ProjectNumber,
                    DocumentType = d.DocumentType,
                    Subject = d.Subject,
                    DateCreated = d.CreateDate,
                    Author = d.Author,
                    DocumentStatus = d.DocumentStatus,
                    FileName = d.Docfilename,
                    FilePath = d.Docfilepath
                })
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if (document == null)
                throw new NotFoundException();

            return document;
        }

        public async Task<bool> CreateAsync(Documents document)
        {
            await _dbContext.Documents.AddAsync(document);
            _dbContext.SaveChanges();

            return true;
        }

        public bool Update(Documents document)
        {
            _dbContext.Documents.Update(document);
            _dbContext.SaveChanges();

            return true;
        }

        public bool Delete(Documents document)
        {
            _dbContext.Documents.Remove(document);
            _dbContext.SaveChanges();

            return true;
        }
    }
}
