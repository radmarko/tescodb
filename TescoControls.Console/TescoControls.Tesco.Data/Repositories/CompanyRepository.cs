﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using TescoControls.Tesco.Data.Context;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Data.Models;
using TescoControls.Utilities.CommonClasses;
using TescoControls.Utilities.Exceptions;

namespace TescoControls.Tesco.Data.Repositories
{
    public class CompanyRepository : BaseRepository
    {
        public CompanyRepository(
            TescoContext tescoContext, 
            ILogger<CompanyRepository> logger) 
            : base(tescoContext, logger)
        {
        }

        public async Task<Company> GetCompanyAsync(int id)
        {
            var company = await _dbContext.Company
                .FirstOrDefaultAsync(c => c.CompanyId == id);

            if (company == null)
                throw new NotFoundException();

            return company;
        }

        public async Task<PaginatedList<CompanyDto>> GetAsync(
            string searchPart,
            int offset,
            int pageSize,
            string sortField,
            bool sortOrder)
        {
            var query = _dbContext.Company
                .Where(c => c.Company1.Contains(searchPart))
                .Select(c => new CompanyDto
                {
                    Id = c.CompanyId,
                    Name = c.Company1
                })
                .AsNoTracking();

            var count = await query.CountAsync();

            if (count == 0)
                throw new NotFoundException(); //todo: add additional data to this constructor

            switch (sortField)
            {
                case "name":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Name)
                            : query.OrderBy(q => q.Name);
                        break;
                    }
                default:
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Id)
                            : query.OrderBy(q => q.Id);
                        break;
                    }
            }

            var records = await query
                .Skip(offset)
                .Take(pageSize)
                .ToListAsync();

            var result = new PaginatedList<CompanyDto>
            {
                Records = records,
                Count = count
            };

            return result;
        }

        public PaginatedList<CompanyDto> Get(
            string searchPart,
            int offset,
            int pageSize,
            string sortField,
            bool sortOrder)
        {
            var query = _dbContext.Company
                .Where(c => c.Company1.Contains(searchPart))
                .Select(c => new CompanyDto
                {
                    Id = c.CompanyId,
                    Name = c.Company1
                })
                .AsNoTracking();

            var count = query.Count();

            if (count == 0)
                throw new NotFoundException(); //todo: add additional data to this constructor

            switch (sortField)
            {
                case "name":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Name)
                            : query.OrderBy(q => q.Name);
                        break;
                    }
                default:
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Id)
                            : query.OrderBy(q => q.Id);
                        break;
                    }
            }

            var records = query
                .Skip(offset)
                .Take(pageSize)
                .ToList();

            var result = new PaginatedList<CompanyDto>
            {
                Records = records,
                Count = count
            };

            return result;
        }

        public async Task<CompanyDto> GetAsync(int id)
        {
            var company = await _dbContext.Company
                .Where(c => c.CompanyId == id)
                .Select(c => new CompanyDto
                {
                    Id = c.CompanyId,
                    Name = c.Company1
                })
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if (company == null)
                throw new NotFoundException();

            return company;
        }

        public async Task<bool> CreateAsync(Company company)
        {
            await _dbContext.Company.AddAsync(company);
            _dbContext.SaveChanges();

            return true;
        }

        public bool Update(Company company)
        {
            _dbContext.Company.Update(company);
            _dbContext.SaveChanges();

            return true;
        }

        public bool Delete(Company company)
        {
            _dbContext.Company.Remove(company);
            _dbContext.SaveChanges();

            return true;
        }
    }
}
