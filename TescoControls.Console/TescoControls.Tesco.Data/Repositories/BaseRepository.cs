﻿using Microsoft.Extensions.Logging;
using TescoControls.Tesco.Data.Context;

namespace TescoControls.Tesco.Data.Repositories
{
    public class BaseRepository
    {
        protected TescoContext _dbContext;
        protected ILogger _logger;

        protected BaseRepository(
            TescoContext tescoContext,
            ILogger logger)
        {
            _dbContext = tescoContext;
            _logger = logger;
        }
    }
}
