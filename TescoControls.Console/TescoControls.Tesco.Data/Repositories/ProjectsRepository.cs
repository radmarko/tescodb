﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using TescoControls.Tesco.Data.Context;
using TescoControls.Tesco.Data.Dtos;
using TescoControls.Tesco.Data.Models;
using TescoControls.Utilities.CommonClasses;
using TescoControls.Utilities.Exceptions;

namespace TescoControls.Tesco.Data.Repositories
{
    public class ProjectsRepository : BaseRepository
    {
        public ProjectsRepository(
            TescoContext tescoContext, 
            ILogger<ProjectsRepository> logger) 
            : base(tescoContext, logger)
        {
        }

        public async Task<Projects> GetProjectAsync(string number)
        {
            var company = await _dbContext.Projects
                .FirstOrDefaultAsync(p => p.ProjectNumber.Equals(number));

            if (company == null)
                throw new NotFoundException();

            return company;
        }

        public async Task<PaginatedList<ProjectDto>> GetAsync(
            string searchPart,
            int offset,
            int pageSize,
            string sortField,
            bool sortOrder)
        {
            var query = _dbContext.Projects
                .Where(p => p.ProjectName.Contains(searchPart))
                .Select(p => new ProjectDto
                {
                    Number = p.ProjectNumber,
                    Name = p.ProjectName,
                    Po = p.ProjectPo,
                    Owner = p.ProjectOwner,
                    ProjectState = p.ProjectState,
                    Address1 = p.PrjAddr1,
                    Address2 = p.PrjAddr2,
                    City = p.PrjCity,
                    State = p.PrjState,
                    Zip = p.PrjZip,
                    Phone = p.PrjPhone
                })
                .AsNoTracking();

            var count = await query.CountAsync();

            if (count == 0)
                throw new NotFoundException(); //todo: add additional data to this constructor

            switch (sortField)
            {
                case "name":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Name)
                            : query.OrderBy(q => q.Name);
                        break;
                    }
                case "owner":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Owner)
                            : query.OrderBy(q => q.Owner);
                        break;
                    }
                case "city":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.City)
                            : query.OrderBy(q => q.City);
                        break;
                    }
                case "state":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.State)
                            : query.OrderBy(q => q.State);
                        break;
                    }
                case "zip":
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Zip)
                            : query.OrderBy(q => q.Zip);
                        break;
                    }
                default:
                    {
                        query = sortOrder ? query.OrderByDescending(q => q.Number)
                            : query.OrderBy(q => q.Number);
                        break;
                    }
            }

            var records = await query
                .Skip(offset)
                .Take(pageSize)
                .ToListAsync();

            var result = new PaginatedList<ProjectDto>
            {
                Records = records,
                Count = count
            };

            return result;
        }

        public async Task<ProjectDto> GetAsync(string number)
        {
            var project = await _dbContext.Projects
                .Where(p => p.ProjectNumber.Equals(number))
                .Select(p => new ProjectDto
                {
                    Number = p.ProjectNumber,
                    Name = p.ProjectName,
                    Po = p.ProjectPo,
                    Owner = p.ProjectOwner,
                    ProjectState = p.ProjectState,
                    Address1 = p.PrjAddr1,
                    Address2 = p.PrjAddr2,
                    City = p.PrjCity,
                    State = p.PrjState,
                    Zip = p.PrjZip,
                    Phone = p.PrjPhone
                })
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if (project == null)
                throw new NotFoundException();

            return project;
        }

        public async Task<bool> CreateAsync(Projects project)
        {
            await _dbContext.Projects.AddAsync(project);
            _dbContext.SaveChanges();

            return true;
        }

        public bool Update(Projects project)
        {
            _dbContext.Projects.Update(project);
            _dbContext.SaveChanges();

            return true;
        }

        public bool Delete(Projects project)
        {
            _dbContext.Projects.Remove(project);
            _dbContext.SaveChanges();

            return true;
        }
    }
}
