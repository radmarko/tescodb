﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class OtherInformation
    {
        public int PrimaryKeyId { get; set; }
        public string ReleaseNotesLink { get; set; }
        public string ReportIssueLink { get; set; }
        public string TescoCompanyDrive { get; set; }
        public bool? SendEmailFromSvr { get; set; }
        public string TrimaxCompanyDrive { get; set; }
    }
}
