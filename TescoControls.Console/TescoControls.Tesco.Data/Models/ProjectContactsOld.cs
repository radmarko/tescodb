﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectContactsOld
    {
        public int ProjectContactId { get; set; }
        public int ContactId { get; set; }
        public string ProjectNumber { get; set; }
        public int ContactTypeId { get; set; }
    }
}
