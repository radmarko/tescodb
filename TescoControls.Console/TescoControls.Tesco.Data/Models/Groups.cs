﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class Groups
    {
        public int GroupId { get; set; }
        public string GroupDesc { get; set; }
    }
}
