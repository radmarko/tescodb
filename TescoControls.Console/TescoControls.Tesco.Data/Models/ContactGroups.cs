﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ContactGroups
    {
        public int ContactGroupId { get; set; }
        public int ContactId { get; set; }
        public string GroupDesc { get; set; }
    }
}
