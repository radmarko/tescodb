﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class MsProjectsSearch
    {
        public string ProjectNumber { get; set; }
        public string ProjectName { get; set; }
        public string ProjectOwner { get; set; }
        public string PrjAddr1 { get; set; }
        public string PrjAddr2 { get; set; }
        public string PrjCity { get; set; }
        public string PrjState { get; set; }
        public string PrjZip { get; set; }
        public string ProjectManager { get; set; }
        public string ProjectEngineer { get; set; }
        public string ElectricalEngineer { get; set; }
        public string Salesman { get; set; }
        public string FieldEngineer { get; set; }
        public string Programmer { get; set; }
        public string SystemsProgrammer { get; set; }
        public string Drafter { get; set; }
        public string Om { get; set; }
        public string EmassTechnician { get; set; }
        public string SegEngineer { get; set; }
    }
}
