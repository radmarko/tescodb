﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class TblVendors
    {
        public int VendorId { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Pager { get; set; }
        public string Title { get; set; }
        public string Altaddr1 { get; set; }
        public string Altaddr2 { get; set; }
        public string Altcity { get; set; }
        public string Altstate { get; set; }
        public string Altzip { get; set; }
        public string AltPhone { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Ext { get; set; }
        public string AltphoneExt { get; set; }
        public string Department { get; set; }
        public string Url { get; set; }
        public string CreateDate { get; set; }
        public string Email { get; set; }
    }
}
