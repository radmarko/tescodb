﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwJohnCompanyHistoryLatest
    {
        public string Company { get; set; }
        public string MaxPrjNum { get; set; }
        public DateTime? MaxPrjDate { get; set; }
        public string CatCode { get; set; }
    }
}
