﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectContactTypes
    {
        public int ContactTypeId { get; set; }
        public string ContactType { get; set; }
    }
}
