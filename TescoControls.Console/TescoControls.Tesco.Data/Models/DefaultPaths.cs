﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class DefaultPaths
    {
        public int KeyId { get; set; }
        public string TescoCompanyDrive { get; set; }
        public string TescoCompanyDir { get; set; }
        public string TescoReportDir { get; set; }
        public string TrimaxCompanyDrive { get; set; }
        public string TrimaxCompanyDir { get; set; }
        public string TrimaxReportDir { get; set; }
        public string TescoProjectsDir { get; set; }
        public string TrimaxProjectsDir { get; set; }
    }
}
