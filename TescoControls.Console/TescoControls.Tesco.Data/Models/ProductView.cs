﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProductView
    {
        public decimal? Expr2 { get; set; }
        public string Company { get; set; }
    }
}
