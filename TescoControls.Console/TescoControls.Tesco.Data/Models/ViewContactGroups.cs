﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ViewContactGroups
    {
        public int ContactId { get; set; }
        public string Company { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Homephone { get; set; }
        public string Mobilephone { get; set; }
        public string Pager { get; set; }
        public string Title { get; set; }
        public string Altphone { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Ext { get; set; }
        public string AltphoneExt { get; set; }
        public string Department { get; set; }
        public DateTime? Createdate { get; set; }
        public DateTime? Editdate { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string ContactType { get; set; }
        public string Alt1Addr1 { get; set; }
        public string Alt1Addr2 { get; set; }
        public string Alt1City { get; set; }
        public string Alt1State { get; set; }
        public string Alt1Zip { get; set; }
        public string Alt2Addr1 { get; set; }
        public string Alt2Addr2 { get; set; }
        public string Alt2City { get; set; }
        public string Alt2State { get; set; }
        public string Alt2Zip { get; set; }
        public string History { get; set; }
        public string Login { get; set; }
        public bool? TrimaxContact { get; set; }
    }
}
