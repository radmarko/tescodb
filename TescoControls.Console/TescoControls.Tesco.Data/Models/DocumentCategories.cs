﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class DocumentCategories
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool? TrimaxCategory { get; set; }
    }
}
