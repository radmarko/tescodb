﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectContactHistory
    {
        public int HistId { get; set; }
        public string PrjNumber { get; set; }
        public DateTime HistEntryDate { get; set; }
        public string HistContact { get; set; }
        public DateTime HistContactDate { get; set; }
        public string HistRegarding { get; set; }
        public string HistDetail { get; set; }
        public string HistUser { get; set; }
        public string HistPhone { get; set; }
        public string HistSetting { get; set; }
        public string HistDuration { get; set; }
    }
}
