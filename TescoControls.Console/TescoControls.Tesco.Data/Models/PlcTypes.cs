﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class PlcTypes
    {
        public int PlcTypeId { get; set; }
        public string PlcName { get; set; }
        public string PlcFullName { get; set; }
    }
}
