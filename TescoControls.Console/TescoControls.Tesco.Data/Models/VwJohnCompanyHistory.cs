﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwJohnCompanyHistory
    {
        public string ProjectNumber { get; set; }
        public string Company { get; set; }
        public string ProjectCatCodes { get; set; }
        public DateTime? MaxPrjDateOrd { get; set; }
    }
}
