﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectNotes
    {
        public int NoteId { get; set; }
        public string ProjectNumber { get; set; }
        public DateTime Notedate { get; set; }
        public string Notedesc { get; set; }
        public string UserLogin { get; set; }

        public virtual Projects ProjectNumberNavigation { get; set; }
    }
}
