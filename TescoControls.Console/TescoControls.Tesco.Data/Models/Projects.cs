﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class Projects
    {
        public Projects()
        {
            ProjectNotes = new HashSet<ProjectNotes>();
        }

        public string ProjectNumber { get; set; }
        public string ProjectName { get; set; }
        public string ProjectPo { get; set; }
        public string ProjectOwner { get; set; }
        public string ProjectState { get; set; }
        public string PrjAddr1 { get; set; }
        public string PrjAddr2 { get; set; }
        public string PrjCity { get; set; }
        public string PrjState { get; set; }
        public string PrjZip { get; set; }
        public string PrjPhone { get; set; }

        public virtual ICollection<ProjectNotes> ProjectNotes { get; set; }
    }
}
