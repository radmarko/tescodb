﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwActiveNonPedProjectsParts
    {
        public string ProjectNumber { get; set; }
        public int PrjPartsId { get; set; }
        public string ProjectOwner { get; set; }
        public string ProjectName { get; set; }
        public string PrjLocation { get; set; }
        public string ProjectStatus { get; set; }
        public string ProjectCatCodes { get; set; }
        public string ProjectType { get; set; }
        public string PrjStatus { get; set; }
        public string PrjComments { get; set; }
        public string PrjEngRel { get; set; }
        public DateTime? PrjDateOrd { get; set; }
        public DateTime? PrjRqstShipDate { get; set; }
        public DateTime? PrjSchdShipDate { get; set; }
        public string PrjShipStat { get; set; }
        public DateTime? PrjDrawDate { get; set; }
        public string PrjOfa { get; set; }
        public DateTime? PrjApprRcvdDate { get; set; }
        public DateTime? PrjRlsShopDate { get; set; }
        public DateTime? PrjEndNeedreleasedDate { get; set; }
        public DateTime? PrjStartNeedreleasedDate { get; set; }
        public DateTime? PrjHistoryDate { get; set; }
    }
}
