﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectEventTypes
    {
        public ProjectEventTypes()
        {
            ProjectEvents = new HashSet<ProjectEvents>();
        }

        public int ProjectEventTypeId { get; set; }
        public string EventTypeName { get; set; }
        public string EventTypeAbbrev { get; set; }
        public int? SortOrder { get; set; }
        public bool? Scheduled { get; set; }
        public bool? SetDuration { get; set; }

        public virtual ICollection<ProjectEvents> ProjectEvents { get; set; }
    }
}
