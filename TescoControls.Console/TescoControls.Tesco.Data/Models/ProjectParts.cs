﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectParts
    {
        public int PrjPartsId { get; set; }
        public string ProjectNumber { get; set; }
        public string PrjLocation { get; set; }
        public string PrjTypeProducts { get; set; }
        public int? PrjPriContactId { get; set; }
        public int? ProjectTypeId { get; set; }
        public int? ProjectStatusId { get; set; }
        public DateTime? PrjDateOrd { get; set; }
        public DateTime? PrjRqstShipDate { get; set; }
        public DateTime? PrjSchdShipDate { get; set; }
        public string PrjShipStat { get; set; }
        public DateTime? PrjDrawDate { get; set; }
        public string PrjOfa { get; set; }
        public DateTime? PrjApprRcvdDate { get; set; }
        public DateTime? PrjRlsShopDate { get; set; }
        public string PrjPlcCnt { get; set; }
        public string PrjStatus { get; set; }
        public string PrjEngRel { get; set; }
        public string PrjPcntMatl { get; set; }
        public string PrjComments { get; set; }
        public string PrjFieldWork { get; set; }
        public string PrjSubcc { get; set; }
        public string PrjDocsNeeded { get; set; }
        public decimal? PrjContractAmt { get; set; }
        public decimal? PrjBilledAmt { get; set; }
        public decimal? PrjAmtOwed { get; set; }
        public string PrjEngineerEe { get; set; }
        public DateTime? PrjWarrantyDate { get; set; }
        public bool PrjVerifiedReqshipdate { get; set; }
        public DateTime? PrjHistoryDate { get; set; }
        public DateTime? PrjEndNeedreleasedDate { get; set; }
        public DateTime? PrjStartNeedreleasedDate { get; set; }
        public DateTime? PrjMaterialDate { get; set; }
        public string PrjScada { get; set; }
    }
}
