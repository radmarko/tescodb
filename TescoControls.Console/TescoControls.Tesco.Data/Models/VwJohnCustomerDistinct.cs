﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwJohnCustomerDistinct
    {
        public string Company { get; set; }
        public string MaxPrjNum { get; set; }
        public string MinCat { get; set; }
        public DateTime? MaxPrjDate { get; set; }
    }
}
