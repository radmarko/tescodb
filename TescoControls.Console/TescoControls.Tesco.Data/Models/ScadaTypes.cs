﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ScadaTypes
    {
        public int ScadaTypeId { get; set; }
        public string ScadaName { get; set; }
        public string ScadaFullName { get; set; }
    }
}
