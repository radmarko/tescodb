﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwActiveNonPedProjects
    {
        public string ProjectNumber { get; set; }
        public string ProjectOwner { get; set; }
        public string ProjectName { get; set; }
    }
}
