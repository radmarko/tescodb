﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwActiveNonPedProjectsWithTeam
    {
        public string ProjectNumber { get; set; }
        public string ProjectOwner { get; set; }
        public string ProjectName { get; set; }
        public string ProjectManager { get; set; }
        public string ProjectEngineer { get; set; }
        public string ElectricalEngineer { get; set; }
        public string Salesman { get; set; }
        public string FieldEngineer2 { get; set; }
        public string Programmer { get; set; }
        public string SystemsProgrammer { get; set; }
        public string Drafter { get; set; }
        public string Om { get; set; }
        public string ProjectManagerIni { get; set; }
        public string ProjectEngineerIni { get; set; }
        public string ElectricalEngineerIni { get; set; }
        public string SalesmanIni { get; set; }
        public string FieldEngineerIni { get; set; }
        public string FieldEngineer2Ini { get; set; }
        public string ProgrammerIni { get; set; }
        public string SystemsProgrammerIni { get; set; }
        public string DrafterIni { get; set; }
        public string OmIni { get; set; }
        public string FieldEngineer { get; set; }
    }
}
