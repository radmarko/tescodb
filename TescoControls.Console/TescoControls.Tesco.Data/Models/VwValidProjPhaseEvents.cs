﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwValidProjPhaseEvents
    {
        public int? ProjectEventId { get; set; }
        public int FkProjectPhaseId { get; set; }
        public int FkProjectEventTypeId { get; set; }
    }
}
