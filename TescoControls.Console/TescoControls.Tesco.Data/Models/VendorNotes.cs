﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VendorNotes
    {
        public int NoteId { get; set; }
        public int? ContactId { get; set; }
        public DateTime? Notedate { get; set; }
        public string Notedesc { get; set; }
        public string UserLogin { get; set; }
    }
}
