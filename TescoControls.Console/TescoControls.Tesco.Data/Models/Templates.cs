﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class Templates
    {
        public int Templateid { get; set; }
        public int? CategoryId { get; set; }
        public string Description { get; set; }
        public string FilePathName { get; set; }
        public string Doctype { get; set; }
        public string UseSubjectText { get; set; }
        public string SubjectText { get; set; }
        public bool? TrimaxTemplate { get; set; }
    }
}
