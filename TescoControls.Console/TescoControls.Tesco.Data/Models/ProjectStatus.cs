﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectStatus
    {
        public int ProjectStatusId { get; set; }
        public string ProjectStatus1 { get; set; }
        public string ProjectCatCodes { get; set; }
    }
}
