﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VendorGroups
    {
        public int ContactGroupId { get; set; }
        public int ContactId { get; set; }
        public string GroupDesc { get; set; }
    }
}
