﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class InfocenterPrjcontacts
    {
        public string ProjectNumber { get; set; }
        public string ProjectName { get; set; }
        public string Docfilepath { get; set; }
    }
}
