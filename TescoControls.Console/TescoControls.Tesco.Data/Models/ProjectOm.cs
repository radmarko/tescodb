﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectOm
    {
        public int Omid { get; set; }
        public string OmprojectNumber { get; set; }
        public int? OmhardcopyCnt { get; set; }
        public int? OmpdfCnt { get; set; }
        public DateTime? OmtocDate { get; set; }
        public DateTime? OmreleaseDate { get; set; }
        public string Omstatus { get; set; }
        public string Omcomments { get; set; }
        public int? Omshipto { get; set; }
    }
}
