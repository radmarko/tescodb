﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwActiveNonPedProjectEvents
    {
        public int ProjectEventId { get; set; }
        public int PrjPartsId { get; set; }
        public string EventTypeAbbrev { get; set; }
        public string ProjectNumber { get; set; }
        public string ProjectCatCodes { get; set; }
        public DateTime? EventDate { get; set; }
        public string EventSum { get; set; }
        public string EventStatus { get; set; }
    }
}
