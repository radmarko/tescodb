﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class Rfi
    {
        public int RfiId { get; set; }
        public string ProjectNumber { get; set; }
        public string Bic { get; set; }
        public string Response { get; set; }
        public string ActionComments { get; set; }
        public DateTime? PendingStamp { get; set; }
        public DateTime? AnsweredStamp { get; set; }
    }
}
