﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwJohnCurrentJobs
    {
        public string Company { get; set; }
        public string MaxProjectNumber { get; set; }
        public DateTime? MaxPrjDateOrd { get; set; }
        public string MinCatCode { get; set; }
    }
}
