﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwJohnCustomerActiveLastHistory
    {
        public string Company { get; set; }
        public DateTime? MaxPrjDate { get; set; }
        public string MinCat { get; set; }
    }
}
