﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectPhases
    {
        public ProjectPhases()
        {
            ProjectEvents = new HashSet<ProjectEvents>();
        }

        public int ProjectPhaseId { get; set; }
        public string FkprojectNumber { get; set; }
        public string PhaseName { get; set; }
        public double? PhaseNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public bool? Retire { get; set; }

        public virtual ICollection<ProjectEvents> ProjectEvents { get; set; }
    }
}
