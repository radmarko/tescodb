﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class TblProjectTeam
    {
        public string ProjectNumber { get; set; }
        public string ProjectManager { get; set; }
        public string ProjectEngineer { get; set; }
        public string ElectricalEngineer { get; set; }
        public string Salesman { get; set; }
        public string FieldEngineer { get; set; }
        public string FieldEngineer2 { get; set; }
        public string Programmer { get; set; }
        public string SystemsProgrammer { get; set; }
        public string Drafter { get; set; }
        public string Om { get; set; }
        public string ProjectManagerIni { get; set; }
        public string ProjectEngineerIni { get; set; }
        public string ElectricalEngineerIni { get; set; }
        public string SalesmanIni { get; set; }
        public string FieldEngineerIni { get; set; }
        public string FieldEngineer2Ini { get; set; }
        public string ProgrammerIni { get; set; }
        public string SystemsProgrammerIni { get; set; }
        public string DrafterIni { get; set; }
        public string OmIni { get; set; }
        public string SpecialProjEngineer { get; set; }
        public string SpecialProjEngineerIni { get; set; }
        public string EmassTechnician { get; set; }
        public string EmassTechnicianIni { get; set; }
        public string SegEngineer { get; set; }
        public string SegEngineerIni { get; set; }
        public string Subscriber1 { get; set; }
        public string Subscriber1Ini { get; set; }
        public string ProjectManagerNotify { get; set; }
        public string ProjectEngineerNotify { get; set; }
        public string ElectricalEngineerNotify { get; set; }
        public string SalesmanNotify { get; set; }
        public string FieldEngineerNotify { get; set; }
        public string FieldEngineer2Notify { get; set; }
        public string ProgrammerNotify { get; set; }
        public string SystemsProgrammerNotify { get; set; }
        public string DrafterNotify { get; set; }
        public string OmNotify { get; set; }
        public string SpecialProjEngineerNotify { get; set; }
        public string EmassTechnicianNotify { get; set; }
        public string SegEngineerNotify { get; set; }
        public string Subscriber1Notify { get; set; }
    }
}
