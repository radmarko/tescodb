﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectTypes
    {
        public int ProjectTypeId { get; set; }
        public string ProjectType { get; set; }
    }
}
