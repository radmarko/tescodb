﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class DocumentTypes
    {
        public int DocumentTypeId { get; set; }
        public string DocumentType { get; set; }
    }
}
