﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class Users
    {
        public string UserLogin { get; set; }
        public string UserName { get; set; }
        public string UserInitials { get; set; }
        public int? DepartmentId { get; set; }
        public short? Seclevel { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Extension { get; set; }
        public string Faxnum { get; set; }
        public string Signaturefile { get; set; }
        public string Computername { get; set; }
        public string BomcpuName { get; set; }
        public bool? Disabled { get; set; }
        public bool? TrimaxAccess { get; set; }
        public string UserNotification { get; set; }
        public bool? UpdateNotify { get; set; }
    }
}
