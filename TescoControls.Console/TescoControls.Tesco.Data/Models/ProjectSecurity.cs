﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectSecurity
    {
        public int Projectid { get; set; }
        public string Projectnumber { get; set; }
        public string Currentuser { get; set; }
        public string Prevstatus { get; set; }
        public string Newstatus { get; set; }
        public DateTime? Changedate { get; set; }
        public decimal? Prevcontractamount { get; set; }
        public decimal? Newcontractamount { get; set; }
        public string Recnum { get; set; }
    }
}
