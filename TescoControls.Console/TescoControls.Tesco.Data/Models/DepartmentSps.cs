﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class DepartmentSps
    {
        public int DeptSpId { get; set; }
        public int FkDeptartmentId { get; set; }
        public string DisplayName { get; set; }
        public string EventSp { get; set; }
    }
}
