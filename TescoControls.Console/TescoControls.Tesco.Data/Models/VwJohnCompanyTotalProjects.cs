﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class VwJohnCompanyTotalProjects
    {
        public string Company { get; set; }
        public int? CustomerTotalProjects { get; set; }
    }
}
