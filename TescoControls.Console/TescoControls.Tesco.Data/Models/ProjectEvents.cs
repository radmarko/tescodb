﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class ProjectEvents
    {
        public int ProjectEventId { get; set; }
        public int FkProjectPhaseId { get; set; }
        public int FkProjectEventTypeId { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public bool? Retire { get; set; }

        public virtual ProjectEventTypes FkProjectEventType { get; set; }
        public virtual ProjectPhases FkProjectPhase { get; set; }
    }
}
