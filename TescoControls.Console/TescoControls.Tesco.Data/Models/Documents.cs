﻿using System;
using System.Collections.Generic;

namespace TescoControls.Tesco.Data.Models
{
    public partial class Documents
    {
        public int DocumentId { get; set; }
        public string ProjectNumber { get; set; }
        public int? ContactId { get; set; }
        public int? ContactDocnum { get; set; }
        public int? ProjectDocnum { get; set; }
        public string DocumentType { get; set; }
        public int? DocumentTypeSeqno { get; set; }
        public string Subject { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Author { get; set; }
        public string DocumentStatus { get; set; }
        public string Docfilename { get; set; }
        public string Docfilepath { get; set; }
        public decimal? DocCoAmount { get; set; }
        public int? RfiId { get; set; }
    }
}
