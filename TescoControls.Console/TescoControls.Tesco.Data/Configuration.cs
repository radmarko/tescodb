﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TescoControls.Tesco.Data.Context;
using TescoControls.Tesco.Data.Repositories;
using TescoControls.Tesco.Data.UnitOfWork;

namespace TescoControls.Tesco.Data
{
    public static class Configuration
    {
        public static IConfigurationBuilder AddTescoDataJsonFiles(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder
                .AddJsonFile("appsettings.tesco-data.json",
                    optional: false,
                    reloadOnChange: true);

            return configurationBuilder;
        }

        public static IServiceCollection ConfigureTescoDataServices(
            this IServiceCollection services,
            IConfiguration configuration,
            bool isDevelopment)
        {
            if (isDevelopment)
            {
                services
                   .AddDbContext<TescoContext>(options =>
                   {
                       options.UseSqlServer(configuration.GetConnectionString("TescoDev"));
                   }, ServiceLifetime.Scoped);
            }
            else
            {
                services
                    .AddDbContext<TescoContext>(options =>
                    {
                        options.UseSqlServer(configuration.GetConnectionString("Tesco"));
                    }, ServiceLifetime.Scoped);
            }

            services.AddScoped<CompanyRepository>();
            services.AddScoped<ContactsRepository>();
            services.AddScoped<DocumentsRepository>();
            services.AddScoped<ProjectsRepository>();
            
            services.AddScoped<TescoUnitOfWork>();

            return services;
        }
    }
}
