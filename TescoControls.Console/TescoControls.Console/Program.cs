﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using TescoControls.Client.HttpClients;
using TescoControls.Helpers.Converter.ObjectDictionary;
using TescoControls.Helpers.Converters.IterativeConverter;
using TescoControls.Helpers.Converters.JavaScriptConverter;
using TescoControls.Helpers.Converters.JsonConverter;
using TescoControls.Tesco.Lib.Services;
using TescoControls.Utilities.CommonClasses;

namespace TescoControls.Console
{
    public class A
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public B B { get; set; }
        public C C { get; set; }
    }
    public class B
    {
        public int Id { get; set; }
        public C CValueData { get; set; }
    }
    public class C
    {
        public int Value { get; set; }
        public string Data { get; set; }
    }

    class Program
    {
        private static IServiceProvider _serviceProvider;
        private static IConfiguration _configuration;

        private static void SetUpDI()
        {
            //setup our DI
            _configuration = new ConfigurationBuilder()
                .AddTescoConsoleJsonFiles()
                .Build();

            _serviceProvider = new ServiceCollection()
                .ConfigureTescoConsoleServices(_configuration, false)
                .BuildServiceProvider();
        }

        private static void TestConverter()
        {
            var a = new A
            {
                Id = 1,
                Name = "one",
                B = new B
                {
                    Id = 2,
                    CValueData = new C
                    {
                        Value = 3,
                        Data = "three"
                    }
                },
                C = new C
                {
                    Value = 4,
                    Data = "four"
                }
            };

            var dictionary = Converter.ObjectToDictionary(a, typeof(A));
            var translatedA = Converter.DictionaryToObject(dictionary, typeof(A));
            var expandoA = Converter.DictionaryToExpandoObject(dictionary);
        }

        private static void TestJsonConverter()
        {
            var a = new A
            {
                Id = 1,
                Name = "one",
                B = new B
                {
                    Id = 2,
                    CValueData = new C
                    {
                        Value = 3,
                        Data = "three"
                    }
                },
                C = new C
                {
                    Value = 4,
                    Data = "four"
                }
            };

            var jsonObject = JsonConverter.ObjectToJson(a);
            var translatedA = JsonConverter.JsonToObject<A>(jsonObject);
            var expandoA = JsonConverter.JsonToExpandoObject(jsonObject);

            var id = expandoA.Id;
            var name = expandoA.Name;
            var b = expandoA.B;
            var c = expandoA.C;

            var iterativeTranslatedA = IterativeConverter.JsonToDictionary(jsonObject);
        }

        private static void TestJsonConverterWithDictionary()
        {
            var a = new AppSettings();
            a.LoadSampleSet();

            a.SaveUserSettings(@"user-settings.json");

            var b = new AppSettings();
            b.LoadUserSettings(@"user-settings.json");

            b.GetItemsFromSettingsDictionary();
        }

        private static void TestJavaScriptConverter()
        {
            var a = new A
            {
                Id = 1,
                Name = "one",
                B = new B
                {
                    Id = 2,
                    CValueData = new C
                    {
                        Value = 3,
                        Data = "three"
                    }
                },
                C = new C
                {
                    Value = 4,
                    Data = "four"
                }
            };

            var jsonObject = JavaScriptConverter2.ObjectToJson(a);
            var translatedA = JavaScriptConverter2.JsonToDictionary(jsonObject);
        }

        private static void TestDb()
        {
            var companyService = _serviceProvider.GetService<CompanyService>();

            var companies = companyService.GetAsync(
                new Query
                {
                    Search = "",
                    Page = 1,
                    PageSize = 20,
                    SortField = "",
                    SortOrder = false
                }).Result;

            var contactService = _serviceProvider.GetService<ContactsService>();

            var contacts = contactService.GetAsync(
                new Query
                {
                    Search = "",
                    Page = 1,
                    PageSize = 20,
                    SortField = "",
                    SortOrder = false
                }).Result;
        }

        static void Main(string[] args)
        {
            SetUpDI();
            //TestConverter();
            //TestJsonConverter();
            TestJsonConverterWithDictionary();
            //TestJavaScriptConverter();

            //TestDb();

            System.Console.WriteLine("Hello World!");
        }
    }
}
