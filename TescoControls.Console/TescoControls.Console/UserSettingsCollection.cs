﻿using System;
using System.Collections.Generic;

namespace TescoControls.Console
{
    public class AddressData
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int ZipCode { get; set; }
        public string State { get; set; }
    }

    public class UserSettingItem
    {
        public string SettingName { get; set; }
        public string OriginalClassName { get; set; }
        public string OriginalPropertyName { get; set; }
        public Type OriginalPropertyType { get; set; }
        public object Value { get; set; }
    }

    public class UserSettingsCollection
    {
        public Dictionary<string, UserSettingItem> UserSettingsKeyValues { get; set; }

        public void InitializeAddSomeDummyData()
        {
            UserSettingsKeyValues = new Dictionary<string, UserSettingItem>();

            //add user name
            UserSettingsKeyValues.Add(
                "setting-one",
                new UserSettingItem
                {
                    SettingName = "UserName",
                    OriginalClassName = nameof(String),
                    OriginalPropertyName = "User",
                    OriginalPropertyType = typeof(string),
                    Value = "radmarko"
                });

            //add name and surname
            UserSettingsKeyValues.Add(
                "setting-two",
                new UserSettingItem
                {
                    SettingName = "NameAndSurname",
                    OriginalClassName = nameof(String),
                    OriginalPropertyName = "User",
                    OriginalPropertyType = typeof(string),
                    Value = "Marko Radosavljevic"
                });

            //add number score
            UserSettingsKeyValues.Add(
                "setting-three",
                new UserSettingItem
                {
                    SettingName = "NumberScore",
                    OriginalClassName = nameof(Double),
                    OriginalPropertyName = "User",
                    OriginalPropertyType = typeof(double),
                    Value = 31232131231.3123d
                });

            //add address
            UserSettingsKeyValues.Add(
                "setting-four",
                new UserSettingItem
                {
                    SettingName = "NumberScore",
                    OriginalClassName = nameof(AddressData),
                    OriginalPropertyName = "User",
                    OriginalPropertyType = typeof(AddressData),
                    Value = new AddressData
                    {
                        Address1 = "Imagionary 15/5",
                        Address2 = "Imaginopolis",
                        ZipCode = 111111,
                        State = "Imagistate"
                    }
                });
        }
    }
}
