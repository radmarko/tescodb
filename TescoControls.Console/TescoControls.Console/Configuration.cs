﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TescoControls.Tesco.Lib;

namespace TescoControls.Console
{
    public static class Configuration
    {
        public static IConfigurationBuilder AddTescoConsoleJsonFiles(this IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.AddTescoLibJsonFiles();

            return configurationBuilder;
        }

        public static IServiceCollection ConfigureTescoConsoleServices(
            this IServiceCollection services,
            IConfiguration configuration,
            bool isDevelopment)
        {
            services.ConfigureTescoLibServices(configuration, isDevelopment);

            services.AddLogging();

            return services;
        }
    }
}
