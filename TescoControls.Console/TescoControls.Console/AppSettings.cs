﻿using System;
using System.Collections.Generic;
using TescoControls.Helpers.Converters.JsonConverter;

namespace TescoControls.Console
{
    public class Address
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int ZipCode { get; set; }
        public string State { get; set; }
    }

    public class AppSettings
    {
        public Dictionary<string, UserSetting> UserSettings;
        public void LoadSampleSet()
        {
            UserSettings = new Dictionary<string, UserSetting>();

            //add user name
            UserSettings.Add(
                "setting-one",
                new UserSetting
                {
                    SettingName = "UserName",
                    OriginalClassName = nameof(String),
                    OriginalPropertyName = "User",
                    OriginalPropertyType = typeof(string),
                    Value = "radmarko"
                });

            //add name and surname
            UserSettings.Add(
                "setting-two",
                new UserSetting
                {
                    SettingName = "NameAndSurname",
                    OriginalClassName = nameof(String),
                    OriginalPropertyName = "User",
                    OriginalPropertyType = typeof(string),
                    Value = "Marko Radosavljevic"
                });

            //add number score
            UserSettings.Add(
                "setting-three",
                new UserSetting
                {
                    SettingName = "NumberScore",
                    OriginalClassName = nameof(Double),
                    OriginalPropertyName = "User",
                    OriginalPropertyType = typeof(double),
                    Value = 31232131231.3123d
                });

            //add address
            UserSettings.Add(
                "setting-four",
                new UserSetting
                {
                    SettingName = "NumberScore",
                    OriginalClassName = nameof(Address),
                    OriginalPropertyName = "User",
                    OriginalPropertyType = typeof(Address),
                    Value = new Address
                    {
                        Address1 = "Imagionary 15/5",
                        Address2 = "Imaginopolis",
                        ZipCode = 111111,
                        State = "Imagistate"
                    }
                });
        }
        public void SaveUserSettings(string jsonFilePath)
        {
            // Save to JSON generically
            JsonConverter.ObjectToJsonFile(UserSettings, jsonFilePath);
        }
        public void LoadUserSettings(string jsonfilepath)
        {
            // Load from JSON generically
            UserSettings = JsonConverter.ObjectFromJsonFile<Dictionary<string, UserSetting>>(jsonfilepath);
        }
        public Dictionary<string, UserSetting> AddItemsToSettingsDictionary()
        {
            // Load random items into the dictionary
            return null;
        }
        public void GetItemsFromSettingsDictionary()
        {
            // Loop through the dictionary and load each item to it's original location
            // I don't know how to copy a value from 'object' back to it's original type (based on the Type that it was defined as)

            foreach (var userSetting in UserSettings)
            {
                var value = JsonConverter.ObjectToClassObject(
                    userSetting.Value.Value,
                    userSetting.Value.OriginalPropertyType);

                //do something with value
            }
        }
    }
    public class UserSetting
    {
        public string SettingName { get; set; }
        public string OriginalClassName { get; set; }
        public string OriginalPropertyName { get; set; }
        public Type OriginalPropertyType { get; set; }
        public object Value { get; set; }
    }
}
